
namespace TimeSheep.Domain;

public class BoundedPeriod(DateTime startDate, DateTime? endDate) : Period(startDate, endDate)
{
    // base.StartDate is not null since we initialized it with a required DateTime.
#pragma warning disable CS8629 // Nullable value type may be null.
    public new DateTime StartDate => base.StartDate.Value;
#pragma warning restore CS8629 // Nullable value type may be null.
}
