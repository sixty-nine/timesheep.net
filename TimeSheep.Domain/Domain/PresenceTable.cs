namespace TimeSheep.Domain;

public class PresenceTable
{
    private readonly List<Period> _list = [];

    public PresenceTable(IEnumerable<Entry> entries)
    {
        Period? lastPeriod = null;

        foreach (var e in entries)
        {
            var period = new Period(e.Start, e.End);

            if (lastPeriod == null)
            {
                lastPeriod = period;
                continue;
            }

            if (period.Touches(lastPeriod))
            {
                lastPeriod = lastPeriod.Merge(period);
                continue;
            }

            _list.Add(lastPeriod);
            lastPeriod = period;
        }

        if (lastPeriod != null)
        {
            _list.Add(lastPeriod);
        }
    }

    public List<Period> List => _list;
}
