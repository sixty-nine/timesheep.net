namespace TimeSheep.Domain;

public sealed class Entry(
    int id,
    BoundedPeriod period,
    string project,
    string? task = null,
    string? description = null
)
{
    public int Id { get; } = id;

    public BoundedPeriod Period { get; } = period;

    public string Project { get; } = project;

    public string? Task { get; } = task;

    public string? Description { get; } = description;

    public DateTime Start => Period.StartDate;

    public DateTime? End => Period.EndDate;

    public TimeSpan Duration => Period.Duration;
}
