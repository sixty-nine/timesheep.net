using TimeSheep.Domain.Extensions;

namespace TimeSheep.Domain;

public class Period : IEquatable<Period>
{
    private readonly DateTime? _startDate;
    private readonly DateTime? _endDate;

    #region Constructor and factories
    public Period(DateTime? startDate, DateTime? endDate)
    {
        if (startDate > endDate)
        {
            throw new ArgumentException("Period start cannot be greater than its end");
        }

        _startDate = startDate;
        _endDate = endDate;
    }

    public static Period FromDate(
        DateTime? startDate,
        DateTime? endDate,
        bool wholeDay,
        bool wholeWeek,
        bool wholeMonth,
        bool wholeYear
    )
    {
        var day = startDate ?? DateTime.Today;

        if (wholeDay)
        {
            return new Period(day.StartOfDay(), day.EndOfDay());
        }
        else if (wholeWeek)
        {
            return new Period(day.StartOfWeek(), day.EndOfWeek());
        }
        else if (wholeMonth)
        {
            return new Period(day.StartOfMonth(), day.EndOfMonth());
        }
        else if (wholeYear)
        {
            return new Period(day.StartOfYear(), day.EndOfYear());
        }

        return new Period(startDate?.StartOfDay(), endDate?.EndOfDay());
    }

    #endregion

    #region Properties
    public static Period GetDay(DateTime date)
        => new(date.StartOfDay(), date.EndOfDay());

    public static Period GetWeek(DateTime date, DayOfWeek startOfWeek = DayOfWeek.Monday)
        => new(date.StartOfWeek(startOfWeek), date.EndOfWeek(startOfWeek));

    public static Period GetMonth(DateTime date) => new(date.StartOfMonth(), date.EndOfMonth());


    public static Period GetYear(DateTime date) => new(date.StartOfYear(), date.EndOfYear());

    public DateTime? StartDate => _startDate;


    public DateTime? EndDate => _endDate;

    public TimeSpan Duration => StartDate.HasValue && EndDate.HasValue ? EndDate.Value - StartDate.Value : new TimeSpan();

    public bool IsBound => StartDate.HasValue && EndDate.HasValue;

    #endregion

    #region Functions

    /// <summary>
    /// Check if this Period overlaps the other Period    
    /// </summary>
    /// <param name="other"></param>
    /// <returns><True if this Period overlaps the other, and false otherwise/returns>
    public bool Overlaps(Period other)
    {
        if (!IsBound || !other.IsBound)
        {
            throw new ArgumentException("Not all periods are bounded");
        }

#pragma warning disable CS8629 // Nullable value type may be null.
        var startInsideOther = other.DateIsInside(StartDate.Value);
        var endInsideOther = other.DateIsInside(EndDate.Value);
        var otherStartInside = DateIsInside(other.StartDate.Value);
        var otherEndInside = DateIsInside(other.EndDate.Value);
#pragma warning restore CS8629 // Nullable value type may be null.

        return startInsideOther || endInsideOther || otherStartInside || otherEndInside;
    }

    public bool Touches(Period other)
    {
        if (!IsBound || !other.IsBound)
        {
            throw new ArgumentException("Not all periods are bounded");
        }

        return StartDate == other.EndDate || EndDate == other.StartDate;
    }

    public bool Contains(DateTime date)
    {
        if (!IsBound)
        {
            throw new ArgumentException("Not all periods are bounded");
        }

        return date >= StartDate && date <= EndDate;
    }

    public bool DateIsInside(DateTime date)
    {
        if (!IsBound)
        {
            throw new ArgumentException("Not all periods are bounded");
        }

        return date > StartDate && date < EndDate;
    }

    public Period Merge(Period other)
    {
        if (!Touches(other))
        {
            throw new ArgumentException("Cannot merge two Period that are not consecutive");
        }

        return new Period(
            StartDate < other.StartDate ? StartDate : other.StartDate,
            EndDate > other.EndDate ? EndDate : other.EndDate
        );
    }

    public IEnumerable<DateTime> Days
    {
        get
        {
            DateTime[] days = [];

            var day = StartDate;

            while (day <= EndDate)
            {
                days = [.. days, day.Value];
                day = day.Value.AddDays(1);
            }

            return days;
        }
    }

    public IEnumerable<DateTime> WorkingDays
    {
        get
        {
            DateTime[] days = [];

            var day = StartDate;

            while (day <= EndDate)
            {
                if ((day.Value.DayOfWeek != DayOfWeek.Saturday) && (day.Value.DayOfWeek != DayOfWeek.Sunday))
                {
                    days = [.. days, day.Value];
                }

                day = day.Value.AddDays(1);
            }

            return days;
        }
    }
    public override string ToString() => $"Period[{StartDate}, {EndDate}]";

    public bool Equals(Period? other)
    {
        if (other is null) return false;
        if (ReferenceEquals(this, other)) return true;
        return other != null && (other.StartDate == StartDate) && (other.EndDate == EndDate);
    }

    public override bool Equals(object? obj)
    {
        if (obj is null) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != this.GetType()) return false;
        return Equals((Period)obj);
    }

    public override int GetHashCode()
    {
        throw new NotImplementedException();
    }


    #endregion
}
