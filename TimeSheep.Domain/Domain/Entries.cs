namespace TimeSheep.Domain;

public class Entries
{
    private readonly IEnumerable<Entry> _entries;
    private readonly TimeSpan _duration = new();

    public Entries(IEnumerable<Entry> entries)
    {
        _entries = entries;

        foreach (var entry in entries)
        {
            if (entry.End.HasValue)
            {
                _duration += entry.Duration;
            }
        }
    }

    public TimeSpan Duration => _duration;

    public IEnumerable<Entry> List => _entries;

    public IEnumerable<string> Projects
    {
        get
        {
            var projects = List.Select(e => e.Project).Distinct();
            return projects.Any() ? projects : [];
        }
    }

    public BoundedPeriod Period
    {
        get
        {
            var min = List.Min(entry => entry.Start);
            var max = List.Max(entry => entry.Start);

            return new BoundedPeriod(min, max);
        }
    }

    public TimeSpan DayDuration(DateTime day)
    {
        return List
            .Where(entry => entry.Start.Date == day)
            .Select(entry => entry.Duration)
            .Aggregate(new TimeSpan(), (acc, duration) => acc + duration);
    }

    public TimeSpan ProjectDuration(string project)
    {
        return List
            .Where(entry => entry.Project == project)
            .Select(entry => entry.Duration)
            .Aggregate(new TimeSpan(), (acc, duration) => acc + duration);
    }

    public TimeSpan ProjectDayDuration(string project, DateTime day)
    {
        return List
            .Where(entry => (entry.Project == project) && (entry.Start.Date == day))
            .Select(entry => entry.Duration)
            .Aggregate(new TimeSpan(), (acc, duration) => acc + duration);
    }
}
