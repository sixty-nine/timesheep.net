namespace TimeSheep.Domain;

public class ProjectStats
{
    private readonly Dictionary<string, ProjectStat> _stats = [];

    public ProjectStats(IEnumerable<Entry> entries)
    {
        foreach (var entry in entries)
        {
            if (!_stats.ContainsKey(entry.Project))
            {
                _stats.Add(entry.Project, new()
                {
                    Project = entry.Project,
                    Duration = new TimeSpan(),
                });
            }

            _stats[entry.Project].Duration = _stats[entry.Project].Duration.Add(entry.Duration);
        }
    }

    public IEnumerable<ProjectStat> List => _stats.Values;
}
