namespace TimeSheep.Domain;

public class ProjectStat : IEquatable<ProjectStat>
{
    public required string Project { get; set; }
    public required TimeSpan Duration { get; set; }

    public bool Equals(ProjectStat? other)
    {
        if (other is null) return false;
        if (ReferenceEquals(this, other)) return true;
        return other != null && (other.Project == Project) && (other.Duration == Duration);
    }

    public override bool Equals(object? obj)
    {
        if (obj is null) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != this.GetType()) return false;
        return Equals((ProjectStat)obj);
    }

    public override int GetHashCode()
    {
        throw new NotImplementedException();
    }
}
