using System.Globalization;
using System.Text.RegularExpressions;

namespace TimeSheep.Domain.Extensions;

public static class DateTimeExtensions
{
    public static DateTime StartOfDay(this DateTime date)
        => new(date.Year, date.Month, date.Day, 0, 0, 0);

    public static DateTime EndOfDay(this DateTime date)
        => new(date.Year, date.Month, date.Day, 23, 59, 59);

    public static DateTime StartOfWeek(this DateTime date, DayOfWeek startOfWeek = DayOfWeek.Monday)
    {
        int diff = (7 + (date.DayOfWeek - startOfWeek)) % 7;
        return date.AddDays(-1 * diff).Date.StartOfDay();
    }

    public static DateTime EndOfWeek(this DateTime date, DayOfWeek startOfWeek = DayOfWeek.Monday)
        => StartOfWeek(date, startOfWeek).AddDays(6).EndOfDay();

    public static DateTime StartOfMonth(this DateTime date)
        => new DateTime(date.Year, date.Month, 1).StartOfDay();

    public static DateTime EndOfMonth(this DateTime date)
        => new DateTime(date.Year, date.Month, 1).AddMonths(1).AddDays(-1).EndOfDay();

    public static DateTime StartOfYear(this DateTime date)
        => new DateTime(date.Year, 1, 1).StartOfDay();

    public static DateTime EndOfYear(this DateTime date)
        => new DateTime(date.Year, 12, 31).EndOfDay();

    public static DateTime? DateTimeFromString(string? inDate, CultureInfo culture)
    {
        if (inDate == null)
        {
            return null;
        }

        try
        {
            return DateTime.Parse((string)inDate, culture);
        }
        catch (FormatException)
        {
            // Pass through
        }

        var today = DateTime.Today;
        var strDate = inDate.ToLower();

        if (strDate == "today")
        {
            return today;
        }
        else if (strDate == "yesterday" || strDate == "1 day ago")
        {
            return today.AddDays(-1);
        }
        else if (strDate == "last week" || strDate == "1 week ago")
        {
            return today.AddDays(-7);
        }
        else if (strDate == "last month")
        {
            var month = new DateTime(today.Year, today.Month, 1);
            return month.AddMonths(-1);
        }
        else if (strDate == "last year")
        {
            return new DateTime(today.Year - 1, 1, 1);
        }
        else if (strDate == "1 month ago")
        {
            return today.AddMonths(-1);
        }
        else if (strDate == "1 year ago")
        {
            return today.AddYears(-1);
        }

        var date = RegexMatch(@"^(\d+) days ago$", strDate, (int i) => today.AddDays(-i));
        if (date.HasValue)
        {
            return date;
        }

        date = RegexMatch(@"^(\d+) weeks ago$", strDate, (int i) => today.AddDays(-7 * i));
        if (date.HasValue)
        {
            return date;
        }

        date = RegexMatch(@"^(\d+) months ago$", strDate, (int i) => today.AddMonths(-i));
        if (date.HasValue)
        {
            return date;
        }

        date = RegexMatch(@"^(\d+) years ago$", strDate, (int i) => today.AddYears(-i));
        if (date.HasValue)
        {
            return date;
        }

        throw new ArgumentException($"Invalid date: '{inDate}'");
    }

    private static DateTime? RegexMatch(string regex, string input, Func<int, DateTime> closure)
    {
        var today = DateTime.Today;
        var matches = Regex.Match(input, regex);

        if (matches.Success)
        {
            try
            {
                var i = int.Parse(matches.Groups[1].Value);
                return closure(i);
            }
            catch
            {
                // Pass through
            }
        }

        return null;
    }
}
