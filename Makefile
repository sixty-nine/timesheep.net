SHELL := /bin/bash

error:
	@echo "Please choose a target"
	@exit 2

.PHONY: build, run-image, publish, coverage

build:
	dotnet build TimeSheep

# run-image:
# 	docker build -t timesheep -f docker/timesheep-api.dockerfile . && \
# 		docker build --no-cache -t timesheep-frontend -f docker/timesheep-frontend.dockerfile . && \
# 		docker compose up -d 
run-image:
	docker build -t timesheep -f docker/timesheep-api.dockerfile . && \
	docker compose -f docker-compose.yml up -d 

publish:
	dotnet publish -r linux-x64 -c Release --self-contained true
	@rm -rf ./output
	@mkdir ./output
	@mkdir ./output/plugins
	@mkdir ./output/plugins/Jira
	@mkdir ./output/plugins/Hello
	@mkdir ./output/db
	@cp TimeSheep/bin/Release/net8.0/linux-x64/publish/timesheep ./output/
	@cp appsettings.json.dist ./output/appsettings.json
	@PLUGINPATH=$$(realpath ./output/plugins); \
	sed -i "s|PLUGIN-PATH|$$PLUGINPATH|g" ./output/appsettings.json
	@cp db/database.db ./output/db/
	@cp Plugins/HelloPlugin/bin/Release/net8.0/linux-x64/publish/HelloPlugin.dll ./output/plugins/Hello/
	@cp Plugins/JiraPlugin/bin/Release/net8.0/linux-x64/publish/* ./output/plugins/Jira/
	@cp Plugins/JiraPlugin/jira.json.dist ./output/plugins/Jira/jira.json

coverage:
	@rm -rf coverage
	@rm -rf ./TimeSheepTests/TestResults/*
	dotnet test --collect:"XPlat Code Coverage"
	@OUT=$$(ls ./TimeSheepTests/TestResults); \
	reportgenerator \
		-reports:"TimeSheepTests/TestResults/$$OUT/coverage.cobertura.xml" \
		-reporttypes:Html \
		-targetdir:"coverage"
	@rm -rf ./TimeSheepTests/TestResults/*
	@firefox coverage/index.html
