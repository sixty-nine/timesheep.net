# Timesheep JIRA plugin

## Introduction

This plugin transmits the timesheet entries to JIRA worklogs.

The plugin expects the entry project field to contain the JIRA Issue Key.

It ensures that no duplicate worklogs are created by using a SQLite database that is created in the same path where the plugin executable resides.

## Setup

### Plugin

Copy the plugin executable `JiraPlugin.dll` into your timesheep plugins directory.

### Configuration

Copy or create the file `jira.json` to the path where the plugin executable `JiraPlugin.dll` resides. Then edit the values.

You must [create a JIRA API token](https://id.atlassian.com/manage-profile/security/api-tokens) and use it as password.

#### Configuration format

```json
{
    "Server": "https://my-server.atlassian.net",
    "User": "my-user",
    "Password": "my-password"
}
```

## Usage

```sh
timesheep plugin jira --from 20.03.2024 --to 24.03.2024
```
