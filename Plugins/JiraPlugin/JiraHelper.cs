using Atlassian.Jira;
using Atlassian.Jira.Remote;

namespace JiraPlugin;

public class JiraHelper
{
    private readonly Jira _jira;

    public JiraHelper(string serverUrl, string user, string password)
    {
        _jira = Jira.CreateRestClient(serverUrl, user, password);

        if (_jira == null)
        {
            throw new ArgumentException("Cannot connect to JIRA");
        }
    }

    public Issue GetIssue(string issueKey)
    {
        return Task.Run(() => _jira.Issues.GetIssueAsync(issueKey)).GetAwaiter().GetResult();
    }

    public bool IssueExists(string issueKey)
    {
        try
        {
            var _ = GetIssue(issueKey);
            return true;
        }
        catch (ResourceNotFoundException)
        {
            return false;
        }
    }

    public void AddWorklog(string issueKey, string timeSpent)
    {
        var issue = GetIssue(issueKey);
        Task.Run(() => issue.AddWorklogAsync(timeSpent)).GetAwaiter().GetResult();
    }

    public TimeSpan GetWorklog(string issueKey)
    {
        var issue = GetIssue(issueKey);
        return TimeSpan.FromSeconds(issue.TimeTrackingData.TimeSpentInSeconds ?? 0);
    }

    public string TimeSpanToString(TimeSpan duration)
    {
        var str = "";

        // Warning "1d" in JIRA is 8 hours, don't use it.
        if (duration.TotalHours > 0)
        {
            str += $"{(int)duration.TotalHours}h ";
        }

        if (duration.Minutes > 0)
        {
            str += $"{duration.Minutes}m";
        }

        return str;
    }
}
