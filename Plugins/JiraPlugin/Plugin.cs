using System.Reflection;
using Microsoft.Extensions.Configuration;
using Spectre.Console;
using TimeSheepSDK;

namespace JiraPlugin;

public class Plugin : IPlugin
{
    public string Name => "jira";

    public string Description => "Send the timesheet to JIRA. Expects the issue key to be stored in the project field.";

    public void Execute(object data)
    {
        var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        var builder = new ConfigurationBuilder().AddJsonFile($"{path}/jira.json", true, true);
        var config = builder.Build();

        var entries = Entries.DecodeData(data);

        var db = new DatabaseContext();

        db.Database.EnsureCreated();

        var server = config["Server"];
        var user = config["User"];
        var password = config["Password"];
        var itemToProcess = false;

        if ((server == null) || (user == null) || (password == null))
        {
            throw new ApplicationException("JIRA server is not configured");
        }

        var jira = new JiraHelper(server, user, password);

        TimeSpan total = new();
        List<ProcessList> processList = [];

        AnsiConsole.Status()
            .Start("Processing...", ctx =>
            {
                foreach (var entry in entries.List)
                {
                    // Update the status and spinner
                    ctx.Status($"Entry {entry.Id} - {entry.Project}");
                    ctx.SpinnerStyle(Style.Parse("green"));

                    if (db.IsEntryProcessed(entry.Id))
                    {
                        AnsiConsole.MarkupLine($"[yellow]{entry.Id} - {entry.Project}: [/][darkorange]Skipped[/]");
                        continue;
                    }

                    if (!jira.IssueExists(entry.Project))
                    {
                        AnsiConsole.MarkupLine($"[yellow]{entry.Id} - {entry.Project}: [/][red]Not found[/]");
                        processList.Add(new ProcessList
                        {
                            Id = entry.Id,
                            Project = entry.Project,
                            Duration = entry.Duration,
                            Action = ProcessAction.Ignore,
                        });
                        continue;
                    }

                    processList.Add(new ProcessList
                    {
                        Id = entry.Id,
                        Project = entry.Project,
                        Duration = entry.Duration,
                        Action = ProcessAction.Transmit,
                    });
                    total = total.Add(entry.Duration);

                    itemToProcess = true;
                    AnsiConsole.MarkupLine($"[yellow]{entry.Id} - {entry.Project}: [/][green]OK [bold]{entry.Duration}[/][/]");
                }
            });

        Console.WriteLine();

        if (!itemToProcess)
        {
            ProcessList(jira, db, processList, false);
            AnsiConsole.MarkupLine("[red]No entries found to be transmitted[/]");
            return;
        }

        AnsiConsole.MarkupLine("[green]The following items will be transmitted to JIRA:[/]\n");

        foreach (var item in processList)
        {
            if (item.Action == ProcessAction.Transmit)
            {
                AnsiConsole.MarkupLine($"  - [yellow]{item.Id} - {item.Project}[/] → [deepskyblue1]{item.Duration}[/]");
            }
        }

        AnsiConsole.WriteLine();
        if (!AnsiConsole.Confirm("Transmit to JIRA?"))
        {
            ProcessList(jira, db, processList, false);
            Console.WriteLine();
            AnsiConsole.MarkupLine("[red]Cancelled[/]");
            return;
        }

        Console.WriteLine();
        ProcessList(jira, db, processList, true);

        AnsiConsole.WriteLine();
        AnsiConsole.MarkupLine($"[deepskyblue1]Total transmitted: [bold]{total}[/][/]");
    }

    private static void ProcessList(JiraHelper jira, DatabaseContext db, List<ProcessList> processList, bool transmit = false)
    {
        AnsiConsole.Status()
            .Start("Transmitting...", ctx =>
            {
                ctx.SpinnerStyle(Style.Parse("green"));

                foreach (var item in processList)
                {
                    if ((item.Action == ProcessAction.Transmit) && transmit)
                    {
                        var duration = jira.TimeSpanToString(item.Duration);
                        jira.AddWorklog(item.Project, duration);
                        AnsiConsole.MarkupLine($"  - [green]Transmitted[/] [yellow]{item.Id} - {item.Project}[/] → [deepskyblue1]{item.Duration}[/]");
                        db.CreateEntry(item.Id);
                    }

                    if (item.Action == ProcessAction.Ignore)
                    {
                        db.CreateEntry(item.Id);
                    }
                }
            });
    }
}
