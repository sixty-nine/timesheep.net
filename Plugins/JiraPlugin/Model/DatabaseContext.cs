using System.Reflection;
using Microsoft.EntityFrameworkCore;

namespace JiraPlugin;

public class DatabaseContext : DbContext
{
    public DbSet<HandledEntries> HandledEntries { get; set; }


    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        optionsBuilder.UseSqlite($"Data Source={path}/jira.db;");
    }

    public void CreateEntry(int entryId)
    {
        var newHandled = new HandledEntries
        {
            EntryId = entryId
        };
        HandledEntries.Add(newHandled);
        SaveChanges();
    }

    public bool IsEntryProcessed(int entryId)
    {
        return HandledEntries.Where(handled => handled.EntryId == entryId).Any();
    }
}
