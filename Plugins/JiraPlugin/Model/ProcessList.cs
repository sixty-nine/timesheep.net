namespace JiraPlugin;

public enum ProcessAction
{
    Transmit,
    Ignore,
}

public class ProcessList
{
    public required int Id { get; set; }
    public required string Project { get; set; }
    public required TimeSpan Duration { get; set; }
    public required ProcessAction Action { get; set; }
}
