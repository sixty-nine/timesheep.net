using Microsoft.EntityFrameworkCore;

namespace JiraPlugin;

[Index(nameof(EntryId))]
public class HandledEntries
{
    public int Id { get; set; }

    public int EntryId { get; set; }
}
