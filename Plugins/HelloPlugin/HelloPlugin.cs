using TimeSheepSDK;

namespace HelloPlugin;

public class HelloPlugin : IPlugin
{
    public string Name => "hello";

    public string Description => "Say hello TimeSheep";

    public void Execute(object data)
    {
        var entries = Entries.DecodeData(data);
        var duration = entries.Duration;

        Console.Write($"Hello, it looks like you worked ");

        if (duration.Days > 0)
        {
            Console.Write($"{duration.Days} days, and ");
        }
        Console.WriteLine($"{duration.Add(TimeSpan.FromDays(-duration.Days)).ToString(@"hh\:mm")} hours");
    }
}
