'use client';

import { useState, useEffect, useCallback } from 'react';

import {
  ChakraProvider, useToast,
} from '@chakra-ui/react';

import {
  Api,
} from './Api/Api';
import { CreateEntryQuery, EntriesQuery } from './Api/Queries';
import {
  Entry, Stats,
  Presence,
} from './Api/Model';

import PeriodSelector from './Components/Forms/PeriodSelector';
import OutputPanel from './Components/Output/OutputPanel';
import CreateEntry from './Components/Forms/CreateEntry';

export default function Home() {
  const [entries, setEntries] = useState<Entry[]>([]);
  const [presence, setPresence] = useState<Presence[]>([]);
  const [stats, setStats] = useState<Stats[]>([]);
  const [isLoading, setIsLoading] = useState(true);

  const baseUrl = process.env.NEXT_PUBLIC_API_BASE_URL;

  if (!baseUrl) {
    throw new Error('You must define the NEXT_PUBLIC_API_BASE_URL env variable');
  }

  const api = useCallback(() => new Api(baseUrl ?? ''), [baseUrl]);

  const toast = useToast();

  const error = useCallback(() => (e: Error) => toast({
    title: 'Error.',
    description: e.message,
    status: 'error',
    duration: 9000,
    isClosable: true,
  }), [toast]);

  const fetchInfo = useCallback(() => async (query: EntriesQuery = {}) => {
    setIsLoading(true);

    // eslint-disable-next-line no-console
    console.log('fetchInfo', query);

    try {
      const info = await api().getAllInfo(query);
      // eslint-disable-next-line no-console
      console.log('INFO', info);
      setEntries(info.entries);
      setStats(info.stats);
      setPresence(info.presence);
      setIsLoading(false);
    } catch (e) {
      error()(e as Error);
    }
  }, [api, error]);

  const onFormChange = (query: EntriesQuery) => {
    fetchInfo()(query);
  };

  const onCreateEntry = async (query: CreateEntryQuery) => {
    // eslint-disable-next-line no-console
    console.log('CREATE', query);

    try {
      await api().createEntry(query);
      // TODO: fetch info with the same list query as previously
    } catch (e) {
      error()(e as Error);
    }
  };

  useEffect(() => {
    fetchInfo()();
  }, [fetchInfo]);

  return (
    <ChakraProvider>
      <main className='min-h-screen pl-12 pr-12 pt-6'>
        <PeriodSelector onChange={onFormChange} isLoading={isLoading} />
        <CreateEntry onCreate={onCreateEntry} />
        <OutputPanel entries={entries} stats={stats} presence={presence} />
      </main>
    </ChakraProvider>
  );
}
