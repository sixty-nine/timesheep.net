import { parseISO } from 'date-fns/parseISO';
import { format } from 'date-fns/format';

const date = (s: string) => {
  const d = parseISO(s);
  return ({
    date: format(d, process.env.NEXT_PUBLIC_DATE_FORMAT ?? 'MM.dd.yyyy'),
    time: format(d, process.env.NEXT_PUBLIC_TIME_FORMAT ?? 'HH:mm'),
  });
};

export default date;
