import { FormControl as BaseFormControl, FormLabel } from '@chakra-ui/react';

type FormControlProps = {
  label?: string;
  children: JSX.Element;
};

const FormControl = (props: FormControlProps) => (
  <BaseFormControl>
    {props.label && (<FormLabel>{props.label}</FormLabel>)}
    {props.children}
  </BaseFormControl>
);

export default FormControl;
