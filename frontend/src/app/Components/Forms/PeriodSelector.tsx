import { useState } from 'react';

import {
  Box,
  Container,
  Input,
  SimpleGrid,
  Text,
} from '@chakra-ui/react';

import FormControl from './FormControl';
import WholePeriodSwitch, { SwitchType } from './WholePeriodSwitch';
import { EntriesQuery } from '../../Api/Queries';

type PeriodSelectorProps = {
  isLoading: boolean;
  onChange?: (query: EntriesQuery) => void;
};

const PeriodSelector = (props: PeriodSelectorProps) => {
  const [isEndDateDisabled, setIsEndDateDisabled] = useState(false);
  const [query] = useState<EntriesQuery>({});
  const [oldToDate, setOldToDate] = useState<string | undefined>(undefined);

  const onPeriodSwitchChange = (value: SwitchType | null) => {
    if (value !== null) {
      if (query.ToDate) {
        setOldToDate(query.ToDate);
      }
      query.Day = value === SwitchType.Day;
      query.Week = value === SwitchType.Week;
      query.Month = value === SwitchType.Month;
      query.Year = value === SwitchType.Year;
      delete query.ToDate;

      setIsEndDateDisabled(true);
    } else {
      delete query.Day;
      delete query.Week;
      delete query.Month;
      delete query.Year;
      query.ToDate = oldToDate;

      setIsEndDateDisabled(false);
    }

    if (props.onChange) {
      props.onChange(query);
    }
  };

  const onFromDateChange = (value: string) => {
    if (props.onChange) {
      query.FromDate = value;
      props.onChange(query);
    }
  };

  const onToDateChange = (value: string) => {
    if (props.onChange) {
      query.ToDate = !isEndDateDisabled ? value : undefined;
      props.onChange(query);
    }
  };

  return (
    <Container maxW='100%' padding='1rem 2rem' border='1px' borderRadius='md' borderColor='gray.200'>
      <Box mb={4}>
        <Text fontSize='2xl'>Period selection</Text>
      </Box>

      <SimpleGrid columns={2} spacing={4}>
        <FormControl label='Start date'>
          <Input
             size='sm'
            placeholder='Start date'
            onChange={(e) => onFromDateChange(e.target.value)}
          />
        </FormControl>
        <FormControl label='End date'>
          <Input
            size='sm'
            placeholder='End date'
            isDisabled={isEndDateDisabled}
            onChange={(e) => onToDateChange(e.target.value)}
          />
        </FormControl>
      </SimpleGrid>
      <Box mt={4}>
        <FormControl>
          <WholePeriodSwitch onChange={onPeriodSwitchChange} />
        </FormControl>
      </Box>
    </Container>
  );
};

export default PeriodSelector;
