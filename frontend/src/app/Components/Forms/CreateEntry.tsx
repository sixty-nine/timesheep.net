import { ChangeEvent, useState } from 'react';

import {
  Box,
  Button,
  Container,
  Input,
  SimpleGrid,
  Text,
} from '@chakra-ui/react';

import { CreateEntryQuery } from '@/app/Api/Queries';

import FormControl from './FormControl';

type CreateEntryProps = {
  onCreate: (query: CreateEntryQuery) => void;
};

const CreateEntry = (props: CreateEntryProps) => {
  const [query] = useState<CreateEntryQuery>({});

  const onChange = (e: ChangeEvent<HTMLInputElement>, param: keyof CreateEntryQuery) => {
    query[param] = e.target.value;
  };

  return (
    <Container maxW='100%' padding='1rem 2rem' border='1px' borderRadius='md' borderColor='gray.200'>
      <Box mb={4}>
        <Text fontSize='2xl'>Create entry</Text>
      </Box>

      <SimpleGrid columns={2} spacing={4}>
        <FormControl label='Start date'>
          <Input
            size='sm'
            placeholder='Start date'
            isRequired={true}
            onChange={(e) => onChange(e, 'Start') }
          />
        </FormControl>

        <FormControl label='End date'>
          <Input
            size='sm'
            placeholder='End date'
            isRequired={true}
            onChange={(e) => onChange(e, 'End') }
          />
        </FormControl>

        <FormControl label='Project'>
          <Input
            size='sm'
            placeholder='Project'
            isRequired={true}
            onChange={(e) => onChange(e, 'Project') }
          />
        </FormControl>

        <FormControl label='Task'>
          <Input
            size='sm'
            placeholder='Task'
            onChange={(e) => onChange(e, 'Task') }
          />
        </FormControl>
      </SimpleGrid>
      <Box mt={4}>
        <FormControl label='Description'>
          <Input
            onChange={(e) => onChange(e, 'Description') }
            size='sm'
            placeholder='Description'
          />
        </FormControl>
      </Box>
      <Box mt={4} display='flex' justifyContent='flex-end'>
        <Button colorScheme='blue' size='sm' onClick={() => props.onCreate(query)}>
          Create
        </Button>
      </Box>
    </Container>
  );
};

export default CreateEntry;
