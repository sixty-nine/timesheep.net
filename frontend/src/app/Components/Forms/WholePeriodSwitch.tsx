import { useState } from 'react';
import { Checkbox, Stack } from '@chakra-ui/react';
import FormControl from './FormControl';

export enum SwitchType {
  Day = 'day',
  Week = 'week',
  Month = 'month',
  Year = 'year',
}

type WholePeriodSwitchProps = {
  onChange?: (value: SwitchType | null) => void;
};

const defaultIsChecked = {
  [SwitchType.Day]: false,
  [SwitchType.Week]: false,
  [SwitchType.Month]: false,
  [SwitchType.Year]: false,
};

export default function WholePeriodSwitch(props: WholePeriodSwitchProps): JSX.Element {
  const [checkedItems, setCheckedItems] = useState({ ...defaultIsChecked });

  const onChange = (type: SwitchType) => {
    const items = { ...defaultIsChecked };
    items[type] = !checkedItems[type];
    setCheckedItems(items);
    if (props.onChange) {
      props.onChange(items[type] ? type : null);
    }
  };

  return (
    <FormControl>
      <Stack direction='row'>
        <Checkbox
          size='sm'
          isChecked={checkedItems[SwitchType.Day]}
          onChange={() => onChange(SwitchType.Day)}
        >
          Day
        </Checkbox>
        <Checkbox
          size='sm'
          isChecked={checkedItems[SwitchType.Week]}
          onChange={() => onChange(SwitchType.Week)}
        >
          Week
        </Checkbox>
        <Checkbox
          size='sm'
          isChecked={checkedItems[SwitchType.Month]}
          onChange={() => onChange(SwitchType.Month)}
        >
          Month
        </Checkbox>
        <Checkbox
          size='sm'
          isChecked={checkedItems[SwitchType.Year]}
          onChange={() => onChange(SwitchType.Year)}
        >
          Year
        </Checkbox>
      </Stack>
    </FormControl>
  );
}
