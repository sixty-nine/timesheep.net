import {
  Tab,
  TabList,
  TabPanel,
  TabPanels,
  Tabs,
} from '@chakra-ui/react';

import {
  Entry,
  Stats,
  Presence,
} from '../../Api/Model';

import EntriesList from './EntriesList';
import StatsList from './StatsList';
import PresenceList from './PresenceList';

type OutputPanelProps = {
  entries: Entry[];
  stats: Stats[];
  presence: Presence[];
};

const OutputPanel = (props: OutputPanelProps) => (
  <Tabs>
  <TabList>
    <Tab>Entries</Tab>
    <Tab>Presence</Tab>
    <Tab>Stats</Tab>
  </TabList>

  <TabPanels>
    <TabPanel>
      <EntriesList entries={props.entries} />
    </TabPanel>
    <TabPanel>
      <PresenceList presence={props.presence} />
    </TabPanel>
    <TabPanel>
      <StatsList stats={props.stats} />
    </TabPanel>
  </TabPanels>
</Tabs>
);

export default OutputPanel;
