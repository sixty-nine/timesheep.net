import {
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  TableContainer,
} from '@chakra-ui/react';

import { Entry } from '../../Api/Model';
import date from '../../Helper/date';

type EntriesListProps = {
  entries?: Entry[];
};

const EntriesList = (props: EntriesListProps) => {
  const hasEntries = props.entries && props.entries.length > 0;

  if (!hasEntries) {
    return <div className='text-red-600'>No entries!</div>;
  }

  return (
    <div>
      <TableContainer>
        <Table variant='striped'>
          <Thead>
            <Tr>
              <Th>Start</Th>
              <Th>End</Th>
              <Th>Duration</Th>
              <Th>Project</Th>
            </Tr>
          </Thead>
          <Tbody>
            {props.entries?.map(
              (e, index) => (
                <Tr key={index}>
                  <Td>{date(e.start).date}</Td>
                  <Td>{date(e.start).time}</Td>
                  <Td>{date(e.end).time}</Td>
                  <Td>{e.duration}</Td>
                  <Td>{e.project}</Td>
                </Tr>
              ),
            )}
          </Tbody>
        </Table>
      </TableContainer>
    </div>
  );
};

export default EntriesList;
