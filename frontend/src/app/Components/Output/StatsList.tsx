import {
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  TableContainer,
} from '@chakra-ui/react';

import { Stats } from '../../Api/Model';

type StatsListProps = {
  stats?: Stats[];
};

const StatsList = (props: StatsListProps) => {
  const hasStats = props.stats && props.stats.length > 0;

  if (!hasStats) {
    return <div className='text-red-600'>No stats!</div>;
  }

  return (
    <div>
      <TableContainer>
        <Table variant='striped'>
          <Thead>
            <Tr>
              <Th>Project</Th>
              <Th>Duration</Th>
            </Tr>
          </Thead>
          <Tbody>
            {props.stats?.map(
              (e, index) => (
                <Tr key={index}>
                  <Td>{e.project}</Td>
                  <Td>{e.duration}</Td>
                </Tr>
              ),
            )}
          </Tbody>
        </Table>
      </TableContainer>
    </div>
  );
};

export default StatsList;
