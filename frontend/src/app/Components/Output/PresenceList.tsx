import {
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  TableContainer,
} from '@chakra-ui/react';

import { Presence } from '../../Api/Model';
import date from '../../Helper/date';

type PresenceListProps = {
  presence?: Presence[];
};

const PresenceList = (props: PresenceListProps) => {
  const hasPresence = props.presence && props.presence.length > 0;

  if (!hasPresence) {
    return <div className='text-red-600'>No presence!</div>;
  }

  return (
    <div>
      <TableContainer>
        <Table variant='striped'>
          <Thead>
            <Tr>
              <Th>Date</Th>
              <Th>Start</Th>
              <Th>End</Th>
              <Th>Duration</Th>
            </Tr>
          </Thead>
          <Tbody>
            {props.presence?.map(
              (e, index) => (
                <Tr key={index}>
                  <Td>{date(e.start).date}</Td>
                  <Td>{date(e.start).time}</Td>
                  <Td>{date(e.end).time}</Td>
                  <Td>{e.duration}</Td>
                </Tr>
              ),
            )}
          </Tbody>
        </Table>
      </TableContainer>
    </div>
  );
};

export default PresenceList;
