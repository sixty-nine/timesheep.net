import { CreateEntryQuery, EntriesQuery } from './Queries';

const QueryStringBuilder = {
  entries: (query?: EntriesQuery, culture: string = 'fr-CH'): string => {
    const params: string[] = [`Culture=${culture}`];

    if (query?.FromDate) {
      params.push(`FromDate=${query?.FromDate}`);
    }
    if (query?.ToDate) {
      params.push(`ToDate=${query?.FromDate}`);
    }
    if (query?.Day) {
      params.push('Day=true');
    }
    if (query?.Week) {
      params.push('Week=true');
    }
    if (query?.Month) {
      params.push('Month=true');
    }
    if (query?.Year) {
      params.push('Year=true');
    }

    return params.join('&');
  },

  createEntry: (query: CreateEntryQuery, culture: string = 'fr-CH'): string => {
    const params: string[] = [`Culture=${culture}`];

    params.push(`Start=${encodeURI(query.Start)}`);
    params.push(`End=${encodeURI(query.End)}`);
    params.push(`Project=${encodeURI(query.Project)}`);

    if (query.Task) {
      params.push(`Task=${encodeURI(query.Task)}`);
    }
    if (query.Description) {
      params.push(`Description=${encodeURI(query.Description)}`);
    }

    return params.join('&');
  },
};

export default QueryStringBuilder;
