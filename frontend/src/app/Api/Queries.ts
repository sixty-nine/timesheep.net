export type EntriesQuery = {
  FromDate?: string;
  ToDate?: string;
  Day?: boolean;
  Week?: boolean;
  Month?: boolean;
  Year?: boolean;
};

export type CreateEntryQuery = {
  Start?: string;
  End?: string;
  Project?: string;
  Task?: string;
  Description?: string;
};
