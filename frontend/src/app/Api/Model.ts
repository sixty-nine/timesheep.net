export type Entry = {
  start: string;
  end: string;
  project: string;
  task?: string;
  description?: string;
  duration: string;
};

export type Presence = {
  start: string;
  end: string;
  duration: string;
};

export type Stats = {
  project: string;
  duration: string;
};

export type AllInfo = {
  entries: Entry[];
  stats: Stats[];
  presence: Presence[];
};
