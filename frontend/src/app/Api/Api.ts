import {
  Entry, Stats, Presence, AllInfo,
} from './Model';
import { CreateEntryQuery, EntriesQuery } from './Queries';
import QueryStringBuilder from './QueryStringBuilder';

export type GetEntriesResponse = Entry[];

export type GetStatsResponse = Stats[];

export type GetPresenceResponse = Presence[];

const fetchData = async <T extends any[]>(url: string): Promise<T> => {
  try {
    const res = await fetch(url);
    const data = await res.json();
    return data;
  } catch (err) {
    return [] as any;
  }
};

export class Api {
  private readonly basePath: string;

  private culture: string = process.env.NEXT_PUBLIC_CULTURE ?? 'fr-CH';

  constructor(baseUrl: string) {
    this.basePath = baseUrl;
  }

  public getAllInfo = async (query?: EntriesQuery): Promise<AllInfo> => {
    const queryString = QueryStringBuilder.entries(query, this.culture);

    let url = `${this.basePath}/entries?${queryString}`;
    const entries = await fetchData<Entry[]>(url);

    url = `${this.basePath}/stats?${queryString}`;
    const stats = await fetchData<Stats[]>(url);

    url = `${this.basePath}/presence?${queryString}`;
    const presence = await fetchData<Presence[]>(url);

    return {
      entries,
      stats,
      presence,
    };
  };

  public getEntries = async (query?: EntriesQuery): Promise<GetEntriesResponse> => {
    const url = `${this.basePath}/entries?${QueryStringBuilder.entries(query, this.culture)}`;
    const data = await fetchData(url);
    return data;
  };

  public getPresence = async (query?: EntriesQuery): Promise<GetPresenceResponse> => {
    const url = `${this.basePath}/presence?${QueryStringBuilder.entries(query, this.culture)}`;
    const data = await fetchData(url);
    return data;
  };

  public getStats = async (query?: EntriesQuery): Promise<GetStatsResponse> => {
    const url = `${this.basePath}/stats?${QueryStringBuilder.entries(query, this.culture)}`;
    const data = fetchData(url);
    return data;
  };

  public createEntry = async (query: CreateEntryQuery): Promise<void> => {
    const url = `${this.basePath}/entries?${QueryStringBuilder.createEntry(query, this.culture)}`;
    const res = await fetch(url, { method: 'POST' });
    if (!res.ok) {
      throw new Error('Create request failed');
    }
  };
}
