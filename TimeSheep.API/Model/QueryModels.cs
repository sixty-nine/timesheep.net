using System.Globalization;

namespace TimeSheep.API.Model;

public static class QueryModels
{
    public class EntriesListQuery
    {
        public string? FromDate { get; set; }
        public string? ToDate { get; set; }
        public bool Day { get; set; }
        public bool Week { get; set; }
        public bool Month { get; set; }
        public bool Year { get; set; }
        public string? Culture { get; set; }
    }
}
