namespace TimeSheep.API.Model;

public static class ReadModels
{
    public class Entry
    {
        public DateTime Start { get; set; }

        public DateTime? End { get; set; }

        public required string Project { get; set; }

        public string? Task { get; set; }

        public string? Description { get; set; }

        public required TimeSpan Duration { get; set; }
    }

    public class Presence
    {
        public DateTime? Start { get; set; }

        public DateTime? End { get; set; }

        public TimeSpan Duration { get; set; }

        public static List<Presence> FromDomainPresenceTable(Domain.PresenceTable table)
        {
            List<Presence> list = [];

            foreach (var p in table.List)
            {
                list.Add(new Presence
                {
                    Start = p.StartDate,
                    End = p.EndDate,
                    Duration = p.Duration,
                });
            }

            return list;
        }
    }

    public class Stats
    {
        public required string Project { get; set; }

        public TimeSpan Duration { get; set; }

        public static List<Stats> FromDomainStats(Domain.ProjectStats stats)
        {
            List<Stats> list = [];

            foreach (var s in stats.List)
            {
                list.Add(new Stats
                {
                    Project = s.Project,
                    Duration = s.Duration,
                });
            }

            return list;
        }
    }

    public class Entries
    {
        private readonly List<Entry> _entries = [];

        private Entries(List<Entry> entries)
        {
            _entries = entries;
        }

        public static Entries FromDomainEntries(Domain.Entries entries)
        {
            List<Entry> list = [];

            foreach (var entry in entries.List)
            {
                list.Add(new Entry
                {
                    Start = entry.Start,
                    End = entry.End,
                    Project = entry.Project,
                    Task = entry.Task,
                    Description = entry.Description,
                    Duration = entry.Duration,
                });
            }

            return new Entries(list);
        }

        public List<Entry> List => _entries;
    }
}
