using Microsoft.AspNetCore.Rewrite;
using TimeSheep.Application;
using TimeSheep.Application.Persistence;
using TimeSheep.Application.Persistence.Repositories;
using TimeSheep.Domain;

internal class Program
{
    private static void Main(string[] args)
    {
        var configBuilder = new ConfigurationBuilder().AddJsonFile($"appsettings.json", true, true);
        var config = configBuilder.Build();

        var builder = WebApplication.CreateBuilder(args);

        builder.Services.AddSwaggerGen(c =>
            c.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo
            {
                Title = "TimeSheep",
                Version = "v1"
            })
        );

        builder.Services.AddScoped<DatabaseContext>(
            _ => DatabaseContext.UsePostgres(@"User ID=postgres;Password=password;Server=timesheep-db;Port=5432;Database=timesheep;Pooling=true;")
        );
        builder.Services.AddScoped<EntriesRepository>(c => new EntriesRepository(c.GetService<DatabaseContext>()));
        builder.Services.AddScoped<EntriesApplicationService>(c => new EntriesApplicationService(c.GetService<EntriesRepository>()));
        builder.Services.AddScoped<EntriesQueryService>(c => new EntriesQueryService(c.GetService<EntriesRepository>()));
        builder.Services.AddControllers();

        var app = builder.Build();

        app.UseCors(builder => builder
            .AllowAnyOrigin()
            .AllowAnyMethod()
            .AllowAnyHeader()
        );

        if (!app.Environment.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
            app.UseHsts();
        }

        app.MapControllerRoute(
            name: "default",
            pattern: "{controller=EntriesController}/{action=List}",
            defaults: new { Controller = "EntriesController", Action = "List" }
        );

        app.UseRewriter(new RewriteOptions().AddRedirect("^$", "swagger"));

        app.UseSwagger();
        app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "TimeSheep v1"));

        app.Run();
    }
}
