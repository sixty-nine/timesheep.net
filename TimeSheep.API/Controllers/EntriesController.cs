using System.Globalization;
using Microsoft.AspNetCore.Mvc;
using TimeSheep.Application;
using TimeSheep.API.Model;
using static TimeSheep.API.Model.QueryModels;

namespace TimeSheep.API.Controllers;

[Route("/entries")]
public class EntriesController(
    ILogger<EntriesController> logger,
    EntriesQueryService queryService,
    EntriesApplicationService appService
) : Controller
{
    /// <summary>
    /// Get a list of entries for the given query parameters.
    /// </summary>
    /// <param name="query">The parameters describing which entries to return.</param>
    /// <returns>A list of entries.</returns>
    [HttpGet]
    public async Task<IActionResult> List(EntriesListQuery query)
    {
        var contract = ContractFromQuery(query);

        logger.LogInformation("List entries", [contract]);

        var entries = await queryService.GetEntriesAsync(contract);

        return Json(ReadModels.Entries.FromDomainEntries(entries).List);
    }

    [Route("/presence")]
    [HttpGet]
    public async Task<IActionResult> Presence(EntriesListQuery query)
    {
        var contract = ContractFromQuery(query);

        logger.LogInformation("Prensence table", [contract]);

        var presence = await queryService.GetPresenceTableAsync(contract);

        return Json(ReadModels.Presence.FromDomainPresenceTable(presence));
    }

    [Route("/stats")]
    [HttpGet]
    public async Task<IActionResult> Stats(EntriesListQuery query)
    {
        var contract = ContractFromQuery(query);

        logger.LogInformation("Stats", [contract]);

        var stats = await queryService.GetStatsAsync(contract);

        return Json(ReadModels.Stats.FromDomainStats(stats));
    }

    [HttpPost]
    public IActionResult Create([FromBody] Contracts.V1.Create command)
    {
        logger.LogInformation("Create entry", [command]);

        appService.Handle(command);

        return Ok();
    }

    private static Contracts.V1.List ContractFromQuery(EntriesListQuery query)
    {
        return new Contracts.V1.List()
        {
            FromDate = query.FromDate,
            ToDate = query.ToDate,
            Day = query.Day,
            Week = query.Week,
            Month = query.Month,
            Year = query.Year,
            Culture = query.Culture != null ? new CultureInfo(query.Culture) : CultureInfo.InvariantCulture,
        };
    }
}
