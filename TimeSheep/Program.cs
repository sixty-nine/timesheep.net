using Microsoft.Extensions.Configuration;
using Spectre.Console.Cli;
using TimeSheep.Commands;
using Microsoft.Extensions.DependencyInjection;
using TimeSheep.DependencyInjection;
using TimeSheep.Helpers;
using TimeSheep.Application;
using TimeSheep.Application.Persistence.Repositories;
using TimeSheep.Application.Persistence;

namespace TimeSheep;

public static class Program
{
    public static void Main(string[] args)
    {
        // Read config file
        var builder = new ConfigurationBuilder().AddJsonFile($"appsettings.json", true, true);
        var config = new Configuration(builder.Build());

        var registrations = new ServiceCollection();
        // var db = DatabaseContext.UsePostgres(@"User ID=postgres;Password=password;Server=127.0.0.1;Port=5432;Database=timesheep;Pooling=true;");
        var db = DatabaseContext.UseSqlite("Data Source=./db/database-new.db");
        db.Database.EnsureCreated();
        var repo = new EntriesRepository(db);

        registrations.AddSingleton<Configuration>(config);
        registrations.AddSingleton<ConsoleHelper>(new ConsoleHelper());
        registrations.AddSingleton<EntriesApplicationService>(new EntriesApplicationService(repo));
        registrations.AddSingleton<EntriesQueryService>(new EntriesQueryService(repo));

        var registrar = new TypeRegistrar(registrations);

        // Set default culture info
        Thread.CurrentThread.CurrentCulture = config.CultureInfo;
        Thread.CurrentThread.CurrentUICulture = config.CultureInfo;

        var app = new CommandApp(registrar);

        app.Configure(c =>
        {
            c.SetApplicationCulture(config.CultureInfo);

            c.AddCommand<ListCommand>("list")
                .WithAlias("ls")
                .WithDescription("Get the list of entries for a given time period")
                .WithExample(["list", "--from", "15.01.2024", "--to", "today"])
                .WithExample(["list", "--from", "yesterday", "--day"])
                .WithExample(["list", "--from", "\"last month\"", "--month"])
                .WithExample(["list", "--week"])
            ;

            c.AddCommand<StatsCommand>("stats")
                .WithDescription("Get projects statistics")
                .WithExample(["stats", "--from", "15.01.2024", "--to", "today"])
                .WithExample(["stats", "--from", "yesterday", "--day"])
                .WithExample(["stats", "--from", "\"last month\"", "--month"])
                .WithExample(["stats", "--week", "--id"])
            ;

            c.AddCommand<PresenceCommand>("presence")
                .WithDescription("Get statistics about the presence")
                .WithExample(["presence", "--from", "15.01.2024", "--to", "today"])
                .WithExample(["presence", "--from", "yesterday", "--day"])
                .WithExample(["presence", "--from", "\"last month\"", "--month"])
                .WithExample(["presence", "--week"])
            ;

            c.AddCommand<AddCommand>("add")
                .WithDescription("Add a new entry")
                .WithExample(["add", "10:00", "11:00", "\"My Project\""])
                .WithExample(["add", "10:00", "11:00", "\"My Project\""])
                .WithExample(["add", "10:00", "12:15", "PROJ1", "Infrastructure", "\"Docker setup\""])
            ;

            c.AddCommand<BreakdownCommand>("breakdown")
                .WithAlias("bd")
                .WithDescription("Get projects breakdown chart")
                .WithExample(["breakdown", "--from", "15.01.2024", "--to", "today"])
                .WithExample(["breakdown", "--from", "yesterday", "--day"])
                .WithExample(["breakdown", "--from", "\"last month\"", "--month"])
                .WithExample(["breakdown", "--week"])
            ;

            c.AddCommand<CalendarCommand>("calendar")
                .WithAlias("cal")
                .WithDescription("Show the calendar of a given date")
                .WithExample("cal", "15.01.2024")
            ;

            c.AddCommand<PluginCommand>("plugin")
                .WithDescription("Run a timesheep plugin")
                .WithExample(["plugin", "--list"])
                .WithExample(["plugin", "my-plugin", "--from", "15.01.2024", "--to", "today"])
            ;

            c.AddCommand<WeekSummaryCommand>("week")
                .WithDescription("Summary of the week in the given date")
                .WithExample("week", "15.01.2024")
            ;

        });

        app.Run(args);
    }
}
