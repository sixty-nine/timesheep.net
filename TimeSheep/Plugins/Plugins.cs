using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using TimeSheepSDK;

namespace TimeSheep.Plugins;

public class PluginLoader
{
    private static IEnumerable<IPlugin> plugins = [];

    public static IEnumerable<IPlugin> List => plugins;

    public static void LoadPlugins(string[] pluginPaths)
    {
        plugins = pluginPaths.SelectMany(pluginPath =>
        {
            Assembly pluginAssembly = LoadPlugin(pluginPath);
            return CreatePlugin(pluginAssembly);
        }).ToList();
    }

    public static void Execute(string pluginName, TimeSheep.Domain.Entries entries)
    {
        IPlugin? plugin = plugins.FirstOrDefault(c => c.Name == pluginName);

        if (plugin == null)
        {
            throw new ArgumentException($"No such plugin: '{pluginName}'.");
        }

        plugin.Execute(entries);
    }

    private static IEnumerable<IPlugin> CreatePlugin(Assembly assembly)
    {
        int count = 0;

        var test = assembly.GetTypes();
        foreach (Type type in assembly.GetTypes())
        {
            if (typeof(IPlugin).IsAssignableFrom(type))
            {
                if (Activator.CreateInstance(type) is IPlugin result)
                {
                    count++;
                    yield return result;
                }
            }
        }

        if (count == 0)
        {
            string availableTypes = string.Join(",", assembly.GetTypes().Select(t => t.FullName));
#pragma warning disable IL3000 // Avoid accessing Assembly file path when publishing as a single file
            throw new ApplicationException(
                $"Can't find any type which implements IPlugin in {assembly} from {assembly.Location}.\n" +
                $"Available types: {availableTypes}");
#pragma warning restore IL3000
        }
    }

    private static Assembly LoadPlugin(string pluginPath)
    {
        var path = System.AppContext.BaseDirectory;
        // Navigate up to the solution root
        var root = Path.GetFullPath(Path.Combine(
            Path.GetDirectoryName(
                Path.GetDirectoryName(
                    Path.GetDirectoryName(
                        Path.GetDirectoryName(
                            Path.GetDirectoryName(path)
                        )
                    )
                )
            ) ?? ""
        ));

        string pluginLocation = Path.GetFullPath(Path.Combine(root, pluginPath.Replace('\\', Path.DirectorySeparatorChar)));
        PluginLoadContext loadContext = new(pluginLocation);
        return loadContext.LoadFromAssemblyName(new AssemblyName(Path.GetFileNameWithoutExtension(pluginLocation)));
    }
}
