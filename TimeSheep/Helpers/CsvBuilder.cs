using System.Dynamic;
using System.Globalization;
using CsvHelper;
using CsvHelper.Configuration;
using TimeSheep.Domain;

namespace TimeSheep.Helpers;

public static class CsvBuilder
{
    private static readonly Func<CultureInfo, CsvConfiguration> CsvConfig =
        (culture) => new CsvConfiguration(culture)
        {
            ShouldQuote = args => true,
        };

    public static string GetEntriesCsv(Entries entries, CultureInfo culture)
    {
        return GenerateCsv(CsvConfig(culture), entries.List);
    }

    public static string GetStatsCsv(ProjectStats stats, CultureInfo culture)
    {
        return GenerateCsv(CsvConfig(culture), stats.List);
    }

    public static string GetPresenceCsv(PresenceTable presence, CultureInfo culture)
    {
        var csvConfig = CsvConfig(culture);
        csvConfig.MemberTypes = CsvHelper.Configuration.MemberTypes.Fields;

        var list = new List<object>();

        foreach (var e in presence.List)
        {
            dynamic obj = new ExpandoObject();
            obj.Date = $"{e.StartDate:d}";
            obj.Start = $"{e.StartDate:t}";
            obj.End = $"{e.EndDate:t}";
            obj.Duration = e.Duration.ToString();
            list.Add(obj);
        }

        return GenerateCsv(csvConfig, list);
    }

    private static string GenerateCsv(CsvConfiguration csvConfig, System.Collections.IEnumerable data)
    {
        using var writer = new StringWriter();
        using var csv = new CsvWriter(writer, csvConfig);
        csv.WriteRecords(data);
        return writer.ToString();
    }
}
