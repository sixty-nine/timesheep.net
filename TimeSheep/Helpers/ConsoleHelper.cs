using System.Collections.Immutable;
using Spectre.Console;
using TimeSheep.Domain;

namespace TimeSheep.Helpers;

public sealed class ConsoleHelper
{
    public void WriteLogo()
    {
        AnsiConsole.Write(new Panel("[darkorange]Timesheep[/] :ewe:")
        {
            Padding = new Padding(15, 0, 15, 0),
            Border = BoxBorder.Double,
            BorderStyle = new Style(Color.DarkOrange)
        });
        AnsiConsole.WriteLine();
    }

    public void Error(string msg)
    {
        AnsiConsole.MarkupLine($"[red]Error:[/] {msg}\n");
    }

    public void WriteDuration(TimeSpan duration, string label = "Duration")
    {
        AnsiConsole.Markup("  [bold green]Total[/]: ");
        AnsiConsole.MarkupLine($"[deepskyblue1]{duration.TotalHours}[/] hours");
    }

    public Table BuildStatsTable(ProjectStats stats)
    {
        var table = new Table();

        table.AddColumn("Project");
        table.AddColumn("Duration");

        foreach (var e in stats.List)
        {
            table.AddRow([
                new Markup($"[yellow]{e.Project}[/]"),
                new Markup($"[bold blue]{e.Duration.ToString(@"hh\:mm")}[/]"),
            ]);
        }

        return table;
    }

    public Table BuildListTable(Entries entries, bool showIds = false)
    {
        var table = new Table();
        DateTime? lastDate = null;

        if (showIds)
        {
            table.AddColumn("ID");
        }

        table.AddColumn("Date");
        table.AddColumn("Start");
        table.AddColumn("End");
        table.AddColumn("Duration");
        table.AddColumn("Project");
        table.AddColumn("Task");
        table.AddColumn("Description");

        foreach (var e in entries.List)
        {
            var date = e.Start.Date.ToString("d");

            if (lastDate.HasValue && lastDate.Value.Equals(e.Start.Date))
            {
                date = "";
            }

            lastDate = e.Start.Date;

            IEnumerable<Markup> line = [
                new Markup($"[bold yellow]{date}[/]"),
                new Markup($"[blue]{e.Start:t}[/]"),
                new Markup($"[blue]{e.End:t}[/]"),
                new Markup($"[bold blue]{e.Duration.ToString(@"hh\:mm")}[/]"),
                new Markup($"[green]{e.Project}[/]"),
                new Markup($"[green]{e.Task}[/]"),
                new Markup($"[green]{e.Description}[/]")
            ];

            if (showIds)
            {
                line = line.Prepend(new Markup($"[bold blue]{e.Id}[/]"));
            }

            table.AddRow(line);
        }

        return table;
    }

    public Table BuildPresenceTable(PresenceTable presence)
    {
        var table = new Table();
        DateTime? lastDate = null;

        table.AddColumn("Date");
        table.AddColumn("Start");
        table.AddColumn("End");
        table.AddColumn("Duration");

        foreach (var e in presence.List)
        {
            var date = e.StartDate?.Date.ToString("d");

            if (lastDate.HasValue && lastDate.Value.Equals(e.StartDate?.Date))
            {
                date = "";
            }

            lastDate = e.StartDate?.Date;

            table.AddRow([
                new Markup($"[bold yellow]{date}[/]"),
                new Markup($"[bold blue]{e.StartDate:t}[/]"),
                new Markup($"[bold blue]{e.EndDate:t}[/]"),
                new Markup($"[blue]{e.Duration.ToString(@"hh\:mm")}[/]"),
            ]);
        }

        return table;
    }

    public BreakdownChart BuildBreakdownChart(ProjectStats stats)
    {
        var bdc = new BreakdownChart().ShowTagValues(false).Width(60);
        var it = GetRandomColors(stats.List.Count()).GetEnumerator();

        foreach (var e in stats.List.OrderBy(x => x.Duration).Reverse().ToDictionary(x => x.Project, x => x.Duration))
        {
            it.MoveNext();
            bdc.AddItem(e.Key, e.Value.TotalMinutes, it.Current);
        }

        return bdc;
    }

    public void WriteDueTime(Period period, Entries entries, float hoursPerDay, float occupationRate)
    {
        int workingDays = period.IsBound
            ? period.WorkingDays.Count()
            : entries.Period.WorkingDays.Count();

        int hours = (int)(workingDays * hoursPerDay * occupationRate);

        var ts = new TimeSpan(hours, 0, 0);

        AnsiConsole.MarkupLine($"  [bold green]Working days[/]; [deepskyblue1]{workingDays}[/]");
        AnsiConsole.MarkupLine($"  [bold green]Due hours[/]: [deepskyblue1]{ts.TotalHours}h[/]");
        AnsiConsole.MarkupLine($"  [bold green]Done[/]: [deepskyblue1]{entries.Duration.TotalHours}h[/]");
        AnsiConsole.MarkupLine($"  [bold green]Diff[/]: [deepskyblue1]{ts.TotalHours - entries.Duration.TotalHours}h[/]");
    }

    private List<Color> GetRandomColors(int count)
    {
        var colors = new List<Color>();
        var random = new Random();

        for (var i = 0; i < count; i++)
        {
            Color color;
            do
            {
                color = new Color(
                    (byte)random.Next(0, 255),
                    (byte)random.Next(0, 255),
                    (byte)random.Next(0, 255)
                );
            } while (colors.Contains(color));

            colors.Add(color);
        }

        return colors;
    }
}
