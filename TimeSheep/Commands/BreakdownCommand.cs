using Spectre.Console;
using Spectre.Console.Cli;
using TimeSheep.Helpers;
using TimeSheep.Commands.Settings;
using TimeSheep.Application;

namespace TimeSheep.Commands;

public class BreakdownCommand(Configuration config, EntriesQueryService service, ConsoleHelper console) : Command<PeriodSelectionSettings>
{
    public override int Execute(CommandContext context, PeriodSelectionSettings settings)
    {
        var stats = service.GetStats(new Contracts.V1.List
        {
            Culture = config.CultureInfo,
            FromDate = settings.FromDate,
            ToDate = settings.ToDate,
            Day = settings.WholeDay,
            Week = settings.WholeWeek,
            Month = settings.WholeMonth,
            Year = settings.WholeYear,
        });

        console.WriteLogo();

        if (stats.List.Count() == 0)
        {
            console.Error("No stats for the given period");
            return 1;
        }

        AnsiConsole.Write(console.BuildBreakdownChart(stats));
        Console.WriteLine();

        return 0;
    }
}
