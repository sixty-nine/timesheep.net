using Spectre.Console;
using Spectre.Console.Cli;
using TimeSheep.Helpers;
using TimeSheep.Commands.Settings;
using TimeSheep.Domain.Extensions;
using TimeSheep.Application;

namespace TimeSheep.Commands;

public class AddCommand(Configuration config, EntriesApplicationService service, ConsoleHelper console) : Command<AddCommandSettings>
{
    public override int Execute(CommandContext context, AddCommandSettings settings)
    {
        var start = DateTimeExtensions.DateTimeFromString(settings.FromDate, config.CultureInfo);
        var end = DateTimeExtensions.DateTimeFromString(settings.ToDate, config.CultureInfo);

        service.Handle(new Contracts.V1.Create
        {
            Start = settings.FromDate,
            End = settings.ToDate,
            Project = settings.Project,
            Task = settings.Task,
            Description = settings.Description,
            Culture = config.Culture,
        });

        console.WriteLogo();
        AnsiConsole.MarkupLine("[darkorange]Entry created[/]\n");
        AnsiConsole.MarkupLine($"  [blue]Start[/]: [bold yellow]{start:t}[/]");
        AnsiConsole.MarkupLine($"  [blue]End[/]: [bold yellow]{end:t}[/]");
        AnsiConsole.MarkupLine($"  [blue]Project[/]: [bold yellow]{settings.Project.ToUpper()}[/]");

        if (settings.Task != null)
        {
            AnsiConsole.MarkupLine($"  [blue]Task[/]: [bold yellow]{settings.Task}[/]");
        }

        if (settings.Description != null)
        {
            AnsiConsole.MarkupLine($"  [blue]Description[/]: [bold yellow]{settings.Description}[/]");
        }
        Console.WriteLine();

        return 0;
    }
}
