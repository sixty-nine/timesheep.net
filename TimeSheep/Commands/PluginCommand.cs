using Spectre.Console;
using Spectre.Console.Cli;
using TimeSheep.Helpers;
using TimeSheep.Commands.Settings;
using TimeSheep.Plugins;
using TimeSheep.Application;

namespace TimeSheep.Commands;

public class PluginCommand(Configuration config, EntriesQueryService service, ConsoleHelper console) : Command<PluginCommandSettings>
{
    public override int Execute(CommandContext context, PluginCommandSettings settings)
    {
        var entries = service.GetEntries(new Contracts.V1.List
        {
            Culture = config.CultureInfo,
            FromDate = settings.FromDate,
            ToDate = settings.ToDate,
            Day = settings.WholeDay,
            Week = settings.WholeWeek,
            Month = settings.WholeMonth,
            Year = settings.WholeYear,
        });

        console.WriteLogo();

        PluginLoader.LoadPlugins([.. config.Plugins]);

        if (settings.List)
        {
            AnsiConsole.WriteLine("Installed plugins:\n");

            foreach (var plugin in PluginLoader.List)
            {
                AnsiConsole.MarkupLine($"  [bold blue]{plugin.Name}[/] - [green]{plugin.Description}[/]");
            }

            Console.WriteLine();

            return 0;
        }

        if (settings.Name is not null)
        {
            PluginLoader.Execute(settings.Name, entries);
        }

        Console.WriteLine();

        return 0;
    }
}
