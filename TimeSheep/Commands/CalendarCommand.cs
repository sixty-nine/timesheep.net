using Spectre.Console;
using Spectre.Console.Cli;
using TimeSheep.Domain.Extensions;
using TimeSheep.Helpers;
using TimeSheep.Commands.Settings;
using TimeSheep.Application;

namespace TimeSheep.Commands;

public class CalendarCommand(Configuration config, EntriesQueryService service, ConsoleHelper console) : Command<CalendarCommandSettings>
{
    public override int Execute(CommandContext context, CalendarCommandSettings settings)
    {
        var date = DateTimeExtensions.DateTimeFromString(settings.Date, config.CultureInfo) ?? DateTime.Today;
        var entries = service.GetEntries(new Contracts.V1.List
        {
            Culture = config.CultureInfo,
            FromDate = date.StartOfMonth().ToString(),
            ToDate = date.EndOfMonth().ToString(),
        });

        var calendar = new Calendar(date.Year, date.Month);

        calendar.HighlightStyle(Style.Parse("yellow bold"));

        foreach (var entry in entries.List)
        {
            calendar.AddCalendarEvent(entry.Start);
        }

        console.WriteLogo();
        AnsiConsole.Write(calendar);

        return 0;
    }
}
