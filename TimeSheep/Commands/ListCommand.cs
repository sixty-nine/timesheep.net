using Spectre.Console;
using Spectre.Console.Cli;
using TimeSheep.Helpers;
using TimeSheep.Commands.Settings;
using TimeSheep.Application;

namespace TimeSheep.Commands;

public class ListCommand(Configuration config, EntriesQueryService service, ConsoleHelper console) : Command<ListCommandSettings>
{
    public override int Execute(CommandContext context, ListCommandSettings settings)
    {
        var contract = new Contracts.V1.List
        {
            Culture = config.CultureInfo,
            FromDate = settings.FromDate,
            ToDate = settings.ToDate,
            Day = settings.WholeDay,
            Week = settings.WholeWeek,
            Month = settings.WholeMonth,
            Year = settings.WholeYear,
        };
        var period = service.GetPeriod(contract);
        var entries = service.GetEntries(period);

        if (settings.Csv ?? false)
        {
            var csv = CsvBuilder.GetEntriesCsv(entries, config.CultureInfo);
            Console.WriteLine(csv);
            return 0;
        }

        if (!entries.List.Any())
        {
            console.Error("No entries for the given period");
            return 1;
        }

        console.WriteLogo();

        AnsiConsole.Write(console.BuildListTable(entries, settings.Id));
        Console.WriteLine();

        console.WriteDuration(entries.Duration);
        Console.WriteLine();

        console.WriteDueTime(period, entries, config.HourPerDay, config.OccupationRate);
        Console.WriteLine();

        return 0;
    }
}
