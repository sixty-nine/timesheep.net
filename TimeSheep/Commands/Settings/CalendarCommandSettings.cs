using System.ComponentModel;
using Spectre.Console.Cli;

namespace TimeSheep.Commands.Settings;

public sealed class CalendarCommandSettings : CommandSettings
{
    [CommandArgument(0, "[date]")]
    [Description("The date")]
    public required string Date { get; set; }
}
