using System.ComponentModel;
using Spectre.Console;
using Spectre.Console.Cli;

namespace TimeSheep.Commands.Settings;

public class PluginCommandSettings : PeriodSelectionSettings
{
    [CommandArgument(0, "[name]")]
    [Description("Name of the plugin to execute")]
    public string? Name { get; set; }

    [CommandOption("--list")]
    [Description("End date")]
    public bool List { get; set; }

    public override ValidationResult Validate()
    {
        if (!List && (Name is null))
        {
            return ValidationResult.Error("You must provide a plugin name");
        }

        return ValidationResult.Success();
    }
}
