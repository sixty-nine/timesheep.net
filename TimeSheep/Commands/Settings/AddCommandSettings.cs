using System.ComponentModel;
using Spectre.Console.Cli;

namespace TimeSheep.Commands.Settings;

public sealed class AddCommandSettings : CommandSettings
{
    [CommandArgument(0, "<start>")]
    [Description("Start date")]
    public required string FromDate { get; set; }

    [CommandArgument(1, "<end>")]
    [Description("End date")]
    public required string ToDate { get; set; }

    [CommandArgument(2, "<project>")]
    [Description("The project")]
    public required string Project { get; set; }

    [CommandArgument(3, "[task]")]
    [Description("The task")]
    public string? Task { get; set; }

    [CommandArgument(4, "[description]")]
    [Description("The description")]
    public string? Description { get; set; }
}
