using System.ComponentModel;
using Spectre.Console.Cli;

namespace TimeSheep.Commands.Settings;

public class ListCommandSettings : PeriodSelectionSettings
{
    [CommandOption("--csv")]
    [Description("Output CSV data")]
    public bool? Csv { get; set; }

    [CommandOption("--id")]
    [Description("Show entries IDs")]
    public bool Id { get; set; }
}
