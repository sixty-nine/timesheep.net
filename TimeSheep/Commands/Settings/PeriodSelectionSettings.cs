using System.ComponentModel;
using Spectre.Console;
using Spectre.Console.Cli;

namespace TimeSheep.Commands.Settings;

public class PeriodSelectionSettings : CommandSettings
{
    [CommandOption("--from")]
    [Description("Start date")]
    public required string FromDate { get; set; }

    [CommandOption("--to")]
    [Description("End date")]
    public string? ToDate { get; set; }

    [CommandOption("--day")]
    [Description("Entries for the whole day")]
    public bool WholeDay { get; set; }

    [CommandOption("--week")]
    [Description("Entries for the whole week")]
    public bool WholeWeek { get; set; }

    [CommandOption("--month")]
    [Description("Entries for the whole month")]
    public bool WholeMonth { get; set; }

    [CommandOption("--year")]
    [Description("Entries for the whole year")]
    public bool WholeYear { get; set; }
}
