using Spectre.Console;
using Spectre.Console.Cli;
using TimeSheep.Helpers;
using TimeSheep.Commands.Settings;
using TimeSheep.Application;

namespace TimeSheep.Commands;

public class StatsCommand(Configuration config, EntriesQueryService service, ConsoleHelper console) : Command<ListCommandSettings>
{
    public override int Execute(CommandContext context, ListCommandSettings settings)
    {
        var contract = new Contracts.V1.List
        {
            Culture = config.CultureInfo,
            FromDate = settings.FromDate,
            ToDate = settings.ToDate,
            Day = settings.WholeDay,
            Week = settings.WholeWeek,
            Month = settings.WholeMonth,
            Year = settings.WholeYear,
        };
        var period = service.GetPeriod(contract);
        var entries = service.GetEntries(period);
        var stats = service.GetStats(entries);

        if (settings.Csv ?? false)
        {
            var csv = CsvBuilder.GetStatsCsv(stats, config.CultureInfo);
            Console.WriteLine(csv);
            return 0;
        }

        console.WriteLogo();

        if (stats.List.Count() == 0)
        {
            console.Error("No stats for the given period");
            return 1;
        }

        AnsiConsole.Write(console.BuildStatsTable(stats));
        Console.WriteLine();

        console.WriteDuration(entries.Duration);
        Console.WriteLine();

        console.WriteDueTime(period, entries, config.HourPerDay, config.OccupationRate);
        Console.WriteLine();

        return 0;
    }
}
