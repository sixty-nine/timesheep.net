using Spectre.Console;
using Spectre.Console.Cli;
using TimeSheep.Helpers;
using TimeSheep.Commands.Settings;
using TimeSheep.Domain;
using TimeSheep.Domain.Extensions;
using TimeSheep.Application;

namespace TimeSheep.Commands;

public class WeekSummaryCommand(
    Configuration config,
    EntriesQueryService service,
    ConsoleHelper console
) : Command<CalendarCommandSettings>
{
    public override int Execute(CommandContext context, CalendarCommandSettings settings)
    {
        var date = DateTimeExtensions.DateTimeFromString(settings.Date, config.CultureInfo);

        if (!date.HasValue)
        {
            date = DateTime.Today;
        }

        var period = Period.GetWeek(date.Value);
        var entries = service.GetEntries(period);

        var days = period.Days;

        var table = new Table();

        #region Header

        table.AddColumn("");

        foreach (var day in days)
        {
            table.AddColumn($"[bold yellow]{day:ddd}\n{day:d}[/]");
        }

        table.AddColumn("");

        #endregion

        #region Project rows

        string[] row = [];

        foreach (var project in entries.Projects)
        {
            row = [$"[green]{project}[/]"];

            foreach (var day in days)
            {
                var duration = entries.ProjectDayDuration(project, day);

                if (duration != TimeSpan.Zero)
                {
                    row = [.. row, $"[blue]{duration.ToString(@"hh\:mm")}[/]"];
                }
                else
                {
                    row = [.. row, ""];
                }
            }

            row = [.. row, $"[bold deepskyblue1]{entries.ProjectDuration(project).ToString(@"hh\:mm")}[/]"];

            table.AddRow(row);
        }

        #endregion

        #region Day total row

        table.AddEmptyRow();

        row = [""];
        foreach (var day in days)
        {
            var duration = entries.DayDuration(day);
            row = [.. row, $"[bold deepskyblue1]{duration.ToString(@"hh\:mm")}[/]"];
        }

        row = [.. row, ""];
        table.AddRow(row);

        #endregion

        console.WriteLogo();
        AnsiConsole.Write(table);
        Console.WriteLine();

        console.WriteDuration(entries.Duration);
        Console.WriteLine();

        console.WriteDueTime(period, entries, config.HourPerDay, config.OccupationRate);
        Console.WriteLine();

        return 0;
    }
}
