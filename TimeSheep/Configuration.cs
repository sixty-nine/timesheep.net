using System.Globalization;
using Microsoft.Extensions.Configuration;

namespace TimeSheep;

public class Configuration
{
    private readonly List<string> _plugins = [];

    public Configuration(IConfigurationRoot config)
    {
        RawConfig = config;

        HourPerDay = 0;
        var hoursPerDay = config["HourPerDay"];
        if (hoursPerDay != null)
        {
            HourPerDay = float.Parse(hoursPerDay);
        }

        OccupationRate = 0;
        var occupationRate = config["OccupationRate"];
        if (occupationRate != null)
        {
            OccupationRate = float.Parse(occupationRate);
        }

        var cultureName = config["Culture"];

        Culture = cultureName != null
            ? cultureName
            : CultureInfo.InvariantCulture.Name;

        CultureInfo = cultureName != null
            ? new CultureInfo(cultureName)
            : CultureInfo.InvariantCulture;

        var pluginsPath = config["PluginsBasePath"] ?? ".";
        var plugins = config.GetSection("Plugins")
            .GetChildren()
            .ToList();

        foreach (var plugin in plugins)
        {
            var name = config[plugin.Path];

            if (name != null)
            {
                _plugins.Add(Path.Combine(pluginsPath, name));
            }
        }
    }

    public IConfigurationRoot RawConfig { get; }

    public CultureInfo CultureInfo { get; }

    public string Culture { get; }

    public float HourPerDay { get; }

    public float OccupationRate { get; }

    public List<string> Plugins => _plugins;
}
