using TimeSheep.Domain;

namespace TimeSheep.Tests.Domain;

public class EntriesTest
{
    [Fact]
    public void TestConstruction()
    {
        var date = DateTime.Today;

        IEnumerable<Entry> list = [
            new Entry(1, new BoundedPeriod(date, date.AddHours(1)), "PROJECT1"),
            new Entry(2, new BoundedPeriod(date.AddHours(1), date.AddHours(3)), "PROJECT2"),
            new Entry(3, new BoundedPeriod(date.AddDays(1), date.AddDays(1).AddHours(3)), "PROJECT2"),
        ];

        var entries = new Entries(list);

        Assert.Equal(6, entries.Duration.Hours);
        Assert.Equal(["PROJECT1", "PROJECT2"], entries.Projects);
        Assert.Equal(list, entries.List);
        Assert.Equal(new BoundedPeriod(date, date.AddDays(1)), entries.Period);
        Assert.Equal(3, entries.DayDuration(date).Hours);
        Assert.Equal(3, entries.DayDuration(date.AddDays(1)).Hours);
        Assert.Equal(1, entries.ProjectDuration("PROJECT1").Hours);
        Assert.Equal(5, entries.ProjectDuration("PROJECT2").Hours);
        Assert.Equal(2, entries.ProjectDayDuration("PROJECT2", date).Hours);
        Assert.Equal(3, entries.ProjectDayDuration("PROJECT2", date.AddDays(1)).Hours);
    }

    [Fact]
    public void TestConstructionEmpty()
    {
        var entries = new Entries([]);
        Assert.Equal(0, entries.Duration.Hours);
        Assert.Equal([], entries.Projects);
        Assert.Equal([], entries.List);
        Assert.Equal(0, entries.DayDuration(DateTime.Today).Hours);
        Assert.Equal(0, entries.ProjectDayDuration("PROJECT1", DateTime.Today).Hours);
        Assert.Equal(0, entries.ProjectDuration("PROJECT1").Hours);
    }
}
