namespace TimeSheep.Tests.Domain;

using TimeSheep.Domain.Extensions;

public class DateTimeExtensionsTest
{
    [Fact]
    public void StartOfDayTest()
    {
        var today = DateTime.Today;
        var start = today.StartOfDay();

        Assert.Equal(today.Year, start.Year);
        Assert.Equal(today.Month, start.Month);
        Assert.Equal(today.Day, start.Day);
        Assert.Equal(0, start.Hour);
        Assert.Equal(0, start.Minute);
        Assert.Equal(0, start.Second);
    }

    [Fact]
    public void EndOfDayTest()
    {
        var today = DateTime.Today;
        var end = today.EndOfDay();

        Assert.Equal(today.Year, end.Year);
        Assert.Equal(today.Month, end.Month);
        Assert.Equal(today.Day, end.Day);
        Assert.Equal(23, end.Hour);
        Assert.Equal(59, end.Minute);
        Assert.Equal(59, end.Second);
    }

    [Fact]
    public void StartAndEndOfWeekTest()
    {
        // Week starts on Monday
        var today = DateTime.Today;
        var start = today.StartOfWeek(DayOfWeek.Monday);
        var end = today.EndOfWeek(DayOfWeek.Monday);

        Assert.Equal(DayOfWeek.Monday, start.DayOfWeek);
        Assert.True(today >= start);
        Assert.Equal(DayOfWeek.Sunday, end.DayOfWeek);
        Assert.True(today <= end);

        // Week starts on Sunday
        start = today.StartOfWeek(DayOfWeek.Sunday);
        end = today.EndOfWeek(DayOfWeek.Sunday);

        Assert.Equal(DayOfWeek.Sunday, start.DayOfWeek);
        Assert.True(today >= start);
        Assert.Equal(DayOfWeek.Saturday, end.DayOfWeek);
        Assert.True(today <= end);
    }

    [Fact]
    public void StartOfMonthTest()
    {
        var today = DateTime.Today;
        var start = today.StartOfMonth();

        Assert.Equal(today.Year, start.Year);
        Assert.Equal(today.Month, start.Month);
        Assert.Equal(1, start.Day);
        Assert.Equal(0, start.Hour);
        Assert.Equal(0, start.Minute);
        Assert.Equal(0, start.Second);
    }

    [Fact]
    public void EndOfMonthTest()
    {
        var today = DateTime.Today;
        var end = today.EndOfMonth();
        var lastDay = today.StartOfMonth().AddMonths(1).AddDays(-1).Day;

        Assert.Equal(today.Year, end.Year);
        Assert.Equal(today.Month, end.Month);
        Assert.Equal(lastDay, end.Day);
        Assert.Equal(23, end.Hour);
        Assert.Equal(59, end.Minute);
        Assert.Equal(59, end.Second);
    }

    [Fact]
    public void StartOfYearTest()
    {
        var today = DateTime.Today;
        var start = today.StartOfYear();

        Assert.Equal(today.Year, start.Year);
        Assert.Equal(1, start.Month);
        Assert.Equal(1, start.Day);
        Assert.Equal(0, start.Hour);
        Assert.Equal(0, start.Minute);
        Assert.Equal(0, start.Second);
    }

    [Fact]
    public void EndOfYearTest()
    {
        var today = DateTime.Today;
        var end = today.EndOfYear();

        Assert.Equal(today.Year, end.Year);
        Assert.Equal(12, end.Month);
        Assert.Equal(31, end.Day);
        Assert.Equal(23, end.Hour);
        Assert.Equal(59, end.Minute);
        Assert.Equal(59, end.Second);
    }
}
