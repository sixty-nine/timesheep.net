using TimeSheep.Domain;

namespace TimeSheep.Tests.Domain;

public class PresenceTableTest
{
    [Fact]
    public void TestPresenceTable()
    {
        var date = DateTime.Today;

        IEnumerable<Entry> list = [
            new Entry(1, new BoundedPeriod(date, date.AddHours(1)), "PROJECT1"),
            new Entry(2, new BoundedPeriod(date.AddHours(1), date.AddHours(3)), "PROJECT2"),
            new Entry(2, new BoundedPeriod(date.AddHours(4), date.AddHours(5)), "PROJECT2"),
            new Entry(3, new BoundedPeriod(date.AddDays(1), date.AddDays(1).AddHours(3)), "PROJECT2"),
        ];

        var entries = new Entries(list);
        var table = new PresenceTable(entries.List);

        IEnumerable<BoundedPeriod> presence = [
            new BoundedPeriod(date, date.AddHours(3)),
            new BoundedPeriod(date.AddHours(4), date.AddHours(5)),
            new BoundedPeriod(date.AddDays(1), date.AddDays(1).AddHours(3)),
        ];

        Assert.Equal(3, table.List.Count);
        Assert.Equal(presence, table.List);
    }
}
