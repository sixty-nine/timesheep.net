using System.Globalization;
using TimeSheep.Domain.Extensions;

namespace TimeSheep.Tests.Domain;

public class DateTimeHelperTest
{
    private readonly DateTime today = DateTime.Today;

    private readonly CultureInfo culture = new("fr-CH");

    [Fact]
    public void ConvertEuDates()
    {
        Assert.Equal(new DateTime(2024, 1, 15), DateTimeExtensions.DateTimeFromString("15.1.2024", culture));
        Assert.Equal(new DateTime(2024, 1, 15), DateTimeExtensions.DateTimeFromString("15-1-2024", culture));
        Assert.Throws<ArgumentException>(() => DateTimeExtensions.DateTimeFromString("1.15.2024", culture));
        Assert.Throws<ArgumentException>(() => DateTimeExtensions.DateTimeFromString("foobar", culture));
    }

    [Fact]
    public void ConvertUsDates()
    {
        var usCulture = new CultureInfo("en-US");

        Assert.Equal(new DateTime(2024, 1, 15), DateTimeExtensions.DateTimeFromString("1.15.2024", usCulture));
        Assert.Equal(new DateTime(2024, 1, 15), DateTimeExtensions.DateTimeFromString("1-15-2024", usCulture));
        Assert.Throws<ArgumentException>(() => DateTimeExtensions.DateTimeFromString("15.1.2024", usCulture));
    }

    [Fact]
    public void ConvertNull()
    {
        Assert.Null(DateTimeExtensions.DateTimeFromString(null, culture));
    }

    [Fact]
    public void ConvertStrings()
    {
        Assert.Equal(today, DateTimeExtensions.DateTimeFromString("today", culture));
        Assert.Equal(today, DateTimeExtensions.DateTimeFromString("Today", culture));
        Assert.Equal(today, DateTimeExtensions.DateTimeFromString("TODAY", culture));

        Assert.Equal(today.AddDays(-1), DateTimeExtensions.DateTimeFromString("yesterday", culture));

        Assert.Equal(today.AddDays(-7), DateTimeExtensions.DateTimeFromString("last week", culture));

        Assert.Equal(today.AddMonths(-1).StartOfMonth(), DateTimeExtensions.DateTimeFromString("last month", culture));

        Assert.Equal(today.AddYears(-1).StartOfYear(), DateTimeExtensions.DateTimeFromString("last year", culture));
    }

    [Fact]
    public void ConvertStringDays()
    {
        Assert.Equal(today.AddDays(-1), DateTimeExtensions.DateTimeFromString("1 day ago", culture));
        Assert.Equal(today.AddDays(-2), DateTimeExtensions.DateTimeFromString("2 days ago", culture));
        Assert.Equal(today.AddDays(-5), DateTimeExtensions.DateTimeFromString("5 days ago", culture));

        Assert.Throws<ArgumentException>(() => DateTimeExtensions.DateTimeFromString("-1 day ago", culture));
        Assert.Throws<ArgumentException>(() => DateTimeExtensions.DateTimeFromString("-2 days ago", culture));
    }

    [Fact]
    public void ConvertStringWeeks()
    {
        Assert.Equal(today.AddDays(-7), DateTimeExtensions.DateTimeFromString("1 week ago", culture));
        Assert.Equal(today.AddDays(-14), DateTimeExtensions.DateTimeFromString("2 weeks ago", culture));
        Assert.Equal(today.AddDays(-21), DateTimeExtensions.DateTimeFromString("3 weeks ago", culture));
    }

    [Fact]
    public void ConvertStringMonths()
    {
        Assert.Equal(today.AddMonths(-1), DateTimeExtensions.DateTimeFromString("1 month ago", culture));
        Assert.Equal(today.AddMonths(-2), DateTimeExtensions.DateTimeFromString("2 months ago", culture));
        Assert.Equal(today.AddMonths(-5), DateTimeExtensions.DateTimeFromString("5 months ago", culture));
    }

    [Fact]
    public void ConvertStringYears()
    {
        Assert.Equal(today.AddYears(-1), DateTimeExtensions.DateTimeFromString("1 year ago", culture));
        Assert.Equal(today.AddYears(-2), DateTimeExtensions.DateTimeFromString("2 years ago", culture));
        Assert.Equal(today.AddYears(-5), DateTimeExtensions.DateTimeFromString("5 years ago", culture));
    }
}
