namespace TimeSheep.Tests.Domain;

using TimeSheep.Domain;
using TimeSheep.Domain.Extensions;

public class PeriodTest
{
    [Fact]
    public void Invalid_Constructor_Args_Throw_Exception()
    {
        Assert.Throws<ArgumentException>(
            () => new Period(DateTime.Today.AddDays(1), DateTime.Today)
        );
    }

    [Fact]
    public void GetDayTest()
    {
        var today = DateTime.Today;
        var period = Period.GetDay(today);

        Assert.Equal(today.StartOfDay(), period.StartDate);
        Assert.Equal(today.EndOfDay(), period.EndDate);
    }

    [Fact]
    public void GetWeekTest()
    {
        var today = DateTime.Today;
        var period = Period.GetWeek(today);

        Assert.Equal(today.StartOfWeek(), period.StartDate);
        Assert.Equal(today.EndOfWeek(), period.EndDate);
    }

    [Fact]
    public void GetMonthTest()
    {
        var today = DateTime.Today;
        var period = Period.GetMonth(today);

        Assert.Equal(today.StartOfMonth(), period.StartDate);
        Assert.Equal(today.EndOfMonth(), period.EndDate);
    }

    [Fact]
    public void GetYearTest()
    {
        var today = DateTime.Today;
        var period = Period.GetYear(today);

        Assert.Equal(today.StartOfYear(), period.StartDate);
        Assert.Equal(today.EndOfYear(), period.EndDate);
    }

    [Fact]
    public void DurationTest()
    {
        var secondsInADay = 24 * 60 * 60;
        var today = DateTime.Today;

        var period = Period.GetDay(today);
        Assert.Equal(secondsInADay - 1, period.Duration.TotalSeconds);

        period = Period.GetWeek(today);
        Assert.Equal((7 * secondsInADay) - 1, period.Duration.TotalSeconds);

        period = new Period(today, null);
        Assert.Equal(0, period.Duration.TotalSeconds);

        period = new Period(null, today);
        Assert.Equal(0, period.Duration.TotalSeconds);

        period = new Period(null, null);
        Assert.Equal(0, period.Duration.TotalSeconds);
    }

    [Fact]
    public void IsBoundTest()
    {
        var today = DateTime.Today;

        Assert.True(new Period(today, today).IsBound);
        Assert.False(new Period(null, null).IsBound);
        Assert.False(new Period(today, null).IsBound);
        Assert.False(new Period(null, today).IsBound);
    }

    [Fact]
    public void OverlapsTest()
    {
        Period p2;

        var today = DateTime.Today;
        var p1 = new Period(today, today.AddDays(3));

        p2 = new Period(today.AddDays(1), today.AddDays(2));
        Assert.True(p1.Overlaps(p2));
        Assert.True(p2.Overlaps(p1));

        p2 = new Period(today.AddDays(-1), today.AddDays(1));
        Assert.True(p1.Overlaps(p2));
        Assert.True(p2.Overlaps(p1));

        p2 = new Period(today.AddDays(-2), today.AddDays(-1));
        Assert.False(p1.Overlaps(p2));
        Assert.False(p2.Overlaps(p1));

        p2 = new Period(today.AddDays(4), today.AddDays(5));
        Assert.False(p1.Overlaps(p2));
        Assert.False(p2.Overlaps(p1));

        p2 = new Period(null, null);
        Assert.Throws<ArgumentException>(() => p2.Overlaps(p1));
        Assert.Throws<ArgumentException>(() => p1.Overlaps(p2));
    }

    [Fact]
    public void TouchesTest()
    {
        Period p2;

        var today = DateTime.Today;
        var p1 = new Period(today, today.AddDays(1));

        p2 = new Period(today.AddDays(-1), today);
        Assert.True(p1.Touches(p2));
        Assert.True(p2.Touches(p1));

        p2 = new Period(today.AddDays(1), today.AddDays(2));
        Assert.True(p1.Touches(p2));
        Assert.True(p2.Touches(p1));

        p2 = new Period(today.AddDays(-2), today.AddDays(-1));
        Assert.False(p1.Touches(p2));
        Assert.False(p2.Touches(p1));

        p2 = new Period(today.AddDays(2), today.AddDays(3));
        Assert.False(p1.Touches(p2));
        Assert.False(p2.Touches(p1));

        p2 = new Period(null, null);
        Assert.Throws<ArgumentException>(() => p1.Touches(p2));
        Assert.Throws<ArgumentException>(() => p2.Touches(p1));
    }

    [Fact]
    public void ContainsTest()
    {
        var today = DateTime.Today;
        var p = new Period(today, today.AddDays(1));

        Assert.True(p.Contains(today));
        Assert.True(p.Contains(today.AddHours(6)));
        Assert.True(p.Contains(today.AddDays(1)));

        Assert.False(p.Contains(today.AddMinutes(-1)));
        Assert.False(p.Contains(today.AddDays(1).AddMinutes(1)));

        Assert.Throws<ArgumentException>(() => new Period(null, null).Contains(today));
    }

    [Fact]
    public void DateIsInsideTest()
    {
        var today = DateTime.Today;
        var p = new Period(today, today.AddDays(1));

        Assert.True(p.DateIsInside(today.AddHours(6)));

        Assert.False(p.DateIsInside(today));
        Assert.False(p.DateIsInside(today.AddDays(1)));
        Assert.False(p.DateIsInside(today.AddMinutes(-1)));
        Assert.False(p.DateIsInside(today.AddDays(1).AddMinutes(1)));

        Assert.Throws<ArgumentException>(() => new Period(null, null).DateIsInside(today));
    }

    [Fact]
    public void MergeTest()
    {
        var today = DateTime.Today;
        var p1 = new Period(today, today.AddDays(1));
        var p2 = new Period(today.AddDays(1), today.AddDays(2));

        var p3 = p1.Merge(p2);

        Assert.Equal(today, p3.StartDate);
        Assert.Equal(today.AddDays(2), p3.EndDate);

        p2 = new Period(today.AddDays(2), today.AddDays(3));
        Assert.Throws<ArgumentException>(() => p1.Merge(p2));

        p2 = new Period(null, null);
        Assert.Throws<ArgumentException>(() => p1.Merge(p2));
    }
}
