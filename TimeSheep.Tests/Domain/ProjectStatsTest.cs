using TimeSheep.Domain;

namespace TimeSheep.Tests.Domain;

public class ProjectStatsTest
{
    [Fact]
    public void testName()
    {
        var date = DateTime.Today;

        IEnumerable<Entry> list = [
            new Entry(1, new BoundedPeriod(date, date.AddHours(1)), "PROJECT1"),
            new Entry(2, new BoundedPeriod(date.AddHours(1), date.AddHours(3)), "PROJECT2"),
            new Entry(3, new BoundedPeriod(date.AddHours(4), date.AddHours(5)), "PROJECT3"),
            new Entry(4, new BoundedPeriod(date.AddDays(1), date.AddDays(1).AddHours(3)), "PROJECT2"),
        ];

        var entries = new Entries(list);
        var stats = new ProjectStats(entries.List);

        var expectedStats = new List<ProjectStat> {
            new() { Project = "PROJECT1", Duration = new TimeSpan(1, 0, 0) },
            new() { Project = "PROJECT2", Duration = new TimeSpan(5, 0, 0) },
            new() { Project = "PROJECT3", Duration = new TimeSpan(1, 0, 0) },
        };

        Assert.Equal(3, stats.List.Count());
        Assert.Equal(expectedStats, stats.List.ToList());
    }
}
