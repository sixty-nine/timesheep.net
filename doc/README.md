# Timesheep

## Introduction

Timesheep is a simple command line tool to track time.

## Configuration

The timesheep configuration resides in `appsettings.json`.

The file must be placed along with the `timesheep` executable.

```json
{
    "Culture": "fr-CH",
    "HourPerDay": 8,
    "OccupationRate": 0.8,
    "PluginsBasePath": "/full/path/to/plugin/root",
    "Plugins": [
        "HelloPlugin.dll",
        "Jira/JiraPlugin.dll"
    ]
}
```

| Key | Description |
|---|---|
| Culture | A valid culture info to set the dates format |
| HoursPerDay | Number of due hours per day |
| OccupationRate | The percent of occupation. 1 = 100%, 0.8 = 80%, ... |
| PluginsBasePath | The full path to the base directory for plugins |
| Plugins | A list to paths to plugins executables (relative to PluginsBasePath) |

## Dates and periods

### Date format

Dates can be input in the format given in the `Culture` configuration key.

Additionaly special date token can be used:

| Token  | Description |
|---|---|
| today | Today's date |
| yesterday | Yesterday's date |
| last week | One week ago |
| last month | First day of last month |
| last year | First day of last year |
| <x> day(s) ago | x days ago |
| <x> week(s) ago | x weeks ago |
| <x> month(s) ago | x months ago |
| <x> year(s) ago | x years ago |

### Time period

Several commands can be configured to use a given time period.

A time period can be given by the start and end date:

```
--from 15.01.2024 --to 17.01.2024
--from yesterday --to today
```

If the end date is omitted, then it is today.

`--from yesterday --to today` is equivalend to `--from yesterday`.

Additionaly some special flags can be used:

| Flag | Description |
|---|---|
| --day | Whole day |
| --week | Whole week |
| --month | Whole month |
| --year | Whole year |

Those flags can be used alone. They will act on the today date. 

They also can be used in conjunction with the `--from` option and will act relatvely to that date.

To select a single day the following options are equivalent

```
--from 15.01.2024 --to 15.01.2024
--from 15.01.2024 --day
```

## Commands

### Add a new entry

#### Usage

```sh
timesheep add <start> <end> <project> [task] [description]
```

#### Examples

```sh
timesheep add 10:00 11:00 Project "My task" "The description of the task"
timesheep add "21.03.2024 10:00" "21.03.2024 11:00" "My project"
```

### List the entries

#### Usage

```sh
timesheep list [OPTIONS]
```

This command takes the time periods options.

Additionaly `--id` flag can be used to display the entries IDs, and the `--csv` flag can be used to export the entries to CSV.

#### Examples

```sh
timesheep list --from 15.01.2024 --to today
timesheep list --from yesterday --day
timesheep list --from "last month" --month
timesheep list --week --id
timesheep list --from 15.01.2024 --week --csv
```

#### Output

```
┌────────────┬───────┬───────┬──────────┬─────────┬──────┬────────────────┐
│ Date       │ Start │ End   │ Duration │ Project │ Task │ Description    │
├────────────┼───────┼───────┼──────────┼─────────┼──────┼────────────────┤
│ 12.03.2024 │ 10:00 │ 11:00 │ 01:00    │ test    │      │ test           │
│            │ 11:00 │ 12:00 │ 01:00    │ test    │      │                │
│            │ 12:00 │ 12:10 │ 00:10    │ test    │      │                │
│            │ 12:10 │ 12:20 │ 00:10    │ test    │      │                │
│            │ 12:20 │ 12:30 │ 00:10    │ test    │      │                │
│            │ 12:30 │ 12:45 │ 00:15    │ test    │ task │ my description │
│            │ 12:45 │ 13:00 │ 00:15    │ test1   │      │                │
│            │ 20:00 │ 20:30 │ 00:30    │ test1   │      │                │
└────────────┴───────┴───────┴──────────┴─────────┴──────┴────────────────┘

  Total: 3,5 hours

  Due hours: 32h
  Done: 3,5h
  Diff: 28,5h
```

### Statistics about the projects

#### Usage

```sh
timesheep stats [OPTIONS]
```

Thid command takes the same options as `list`.

#### Examples

```sh
timesheep stats --from 15.01.2024 --to today
timesheep stats --from yesterday --day
timesheep stats --from "last month" --month
timesheep stats --week --csv
```

#### Output

```
┌─────────┬──────────┐
│ Project │ Duration │
├─────────┼──────────┤
│ test    │ 02:45    │
│ test1   │ 00:45    │
└─────────┴──────────┘

  Total: 3,5 hours

  Due hours: 32h
  Done: 3,5h
  Diff: 28,5h
```

### Presence table

#### Usage

```sh
timesheep presence [OPTIONS]
```

#### Examples

```sh
timesheep presence --from 15.01.2024 --to today
timesheep presence --from yesterday --day
timesheep presence --from "last month" --month
timesheep presence --week
```

#### Output

```
┌────────────┬───────┬───────┬──────────┐
│ Date       │ Start │ End   │ Duration │
├────────────┼───────┼───────┼──────────┤
│ 12.03.2024 │ 10:00 │ 13:00 │ 03:00    │
│            │ 20:00 │ 20:30 │ 00:30    │
└────────────┴───────┴───────┴──────────┘

  Total: 3,5 hours

  Due hours: 32h
  Done: 3,5h
  Diff: 28,5h
```

 ### Week summary

#### Usage

```sh
timesheep week <date>
```

#### Examples

```sh
timesheep week 15.01.2024
```

#### Output
```
┌───────┬────────────┬────────────┬────────────┬────────────┬────────────┬────────────┬────────────┬───────┐
│       │ lun.       │ mar.       │ mer.       │ jeu.       │ ven.       │ sam.       │ dim.       │       │
│       │ 11.03.2024 │ 12.03.2024 │ 13.03.2024 │ 14.03.2024 │ 15.03.2024 │ 16.03.2024 │ 17.03.2024 │       │
├───────┼────────────┼────────────┼────────────┼────────────┼────────────┼────────────┼────────────┼───────┤
│ test  │            │ 02:45      │            │            │            │            │            │ 02:45 │
│ test1 │            │ 00:45      │            │            │            │            │            │ 00:45 │
│       │            │            │            │            │            │            │            │       │
│       │ 00:00      │ 03:30      │ 00:00      │ 00:00      │ 00:00      │ 00:00      │ 00:00      │       │
└───────┴────────────┴────────────┴────────────┴────────────┴────────────┴────────────┴────────────┴───────┘

  Total: 3,5 hours

  Due hours: 32h
  Done: 3,5h
  Diff: 28,5h
```
