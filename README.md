# Timesheep

Timesheep is a simple extensible command line tool for personal time tracking.

See [the documentation](./doc/README.md).

It includes two plugins:

  * [HelloPlugin](./HelloPlugin/) - Dummy plugin example
  * [JiraPlugin](./JiraPlugin/README.md) - Export timesheet entries to JIRA worklog.

It also includes a [TimeSheepSDK](./TimeSheepSDK/README.md) to implement new plugins.
