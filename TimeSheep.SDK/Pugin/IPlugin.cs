namespace TimeSheepSDK;

public interface IPlugin
{
    string Name { get; }

    string Description { get; }

    void Execute(object data);
}
