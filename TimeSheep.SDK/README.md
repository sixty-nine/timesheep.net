# How to create a TimeSheep plugin

You need the `TimeSheepSDK.dll` in order to reference it in your project.

### Create a new class library in your project:

```sh
dotnet new classlib -o MyPlugin
dotnet sln add MyPlugin/MyPlugin.csproj
```

### Edit the project file `MyPlugin/MyPlugin.csproj`:

Add `<EnableDynamicLoading>true</EnableDynamicLoading>` in the `<PropertyGroup>`.

Reference the TimeSheepsDK:

  ```xml
  <ItemGroup>
    <ProjectReference Include="..\TimeSheepSDK\TimeSheepSDK.csproj">
        <Private>false</Private>
        <ExcludeAssets>runtime</ExcludeAssets>
    </ProjectReference>
  </ItemGroup>
  ```

### Create the plugin

```csharp
using TimeSheepSDK;

namespace MyPlugin;

public class MyPlugin: IPlugin
{
    public string Name => "my-plugin";

    public string Description => "This is my plugin.";

    public void Execute(object data)
    {
        Entries entries = Entries.DecodeData(data);

        // TODO: do something with the timesheet entries.
    }
}

```

### Loading the plugin in Timesheep

```csharp
Plugins.LoadPlugins([
    "<PATH TO MyPlugin.dll>",
]);
Plugins.Execute("my-plugin", entries);
```
