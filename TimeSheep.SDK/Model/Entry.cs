namespace TimeSheepSDK;
public class Entry
{
    public int Id { get; set; }

    public DateTime Start { get; set; }

    public DateTime? End { get; set; }

    public required string Project { get; set; }

    public string? Task { get; set; }

    public string? Description { get; set; }

    public required TimeSpan Duration { get; set; }
}
