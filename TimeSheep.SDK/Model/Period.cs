namespace TimeSheepSDK;

public class Period
{
    public required string StartDate { get; set; }
    public required string EndDate { get; set; }
    public required string Duration { get; set; }
}
