using System.Text.Json;

namespace TimeSheepSDK;

public class Entries
{
    public required TimeSpan Duration { get; set; }

    public required IEnumerable<Entry> List { get; set; }

    public static Entries DecodeData(object data)
    {
        var obj = JsonSerializer.Deserialize<Entries>(JsonSerializer.Serialize(data))
         ?? throw new ApplicationException("Cannot decode the data received from TimeSheep");

        return obj;
    }
}
