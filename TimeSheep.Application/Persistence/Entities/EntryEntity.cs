namespace TimeSheep.Application.Persistence;

public class EntryEntity
{
    public int Id { get; set; }

    public DateTime Start { get; set; }

    public DateTime? End { get; set; }

    public required string Project { get; set; }

    public string? Task { get; set; }

    public string? Description { get; set; }

    public TimeSpan Duration => End.HasValue ? End.Value - Start : new TimeSpan();
}
