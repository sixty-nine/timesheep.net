namespace TimeSheep.Application.Persistence;

public class ProjectEntity
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public string? Description { get; set; }
}
