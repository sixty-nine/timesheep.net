using TimeSheep.Domain;

namespace TimeSheep.Application.Persistence.Repositories;

public interface IEntriesRepository : IDisposable
{
    public Task<Entries> GetEntries(DateTime? start, DateTime? end);
    public Task<Entries> GetCrossingEntries(DateTime start, DateTime end);
    public void AddEntry(
        DateTime start,
        DateTime end,
        string project,
        string? task = null,
        string? description = null
    );
}
