using TimeSheep.Domain;

namespace TimeSheep.Application.Persistence.Repositories;

public class EntriesRepository(DatabaseContext context) : IEntriesRepository
{
    private bool _disposed;

    public DatabaseContext Context { get; } = context;

    public async Task<Entries> GetEntries(DateTime? start, DateTime? end)
    {
        var closure = GetListClosure(start, end);
        var entries = Context.Entries.Where(closure).ToList();

        return EntriesFromEntity(entries);
    }

    public async Task<Entries> GetCrossingEntries(DateTime start, DateTime end)
    {
        bool closure(EntryEntity e) =>
            (e.Start < start && e.End > start) || // start is inside another entry
            (e.Start < end && e.End > end) ||     // end is inside another entry
            (start < e.Start && end > e.Start) || // other start is inside new
            (start < e.End && end > e.End) ||     // other end is inside new
            (start == e.Start && end == e.End);   // it's the same entry

        var entries = Context.Entries.Where(closure).ToList();

        return EntriesFromEntity(entries);
    }

    public async void AddEntry(
        DateTime start,
        DateTime end,
        string project,
        string? task = null,
        string? description = null
    )
    {
        if (end <= start)
        {
            throw new ArgumentException("The end date cannot be before the start date");
        }

        if ((await GetCrossingEntries(start, end)).List.Any())
        {
            throw new ArgumentException("There is another entry crossing the new one");
        }

        var entry = new EntryEntity
        {
            Start = start,
            End = end,
            Project = project,
            Task = task,
            Description = description,
        };

        Context.Entries.Add(entry);
        Context.SaveChanges();
    }

    private static Func<EntryEntity, bool> GetListClosure(DateTime? start, DateTime? end)
    {
        if (start.HasValue && end.HasValue)
        {
            return e => e.Start >= start.Value && e.End <= end.Value;
        }

        if (start.HasValue)
        {
            return e => e.Start >= start.Value;
        }

        if (end.HasValue)
        {
            return e => e.End <= end.Value;
        }

        return _ => true;
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    protected virtual void Dispose(bool disposing)
    {
        if (!_disposed)
            if (disposing)
                Context.Dispose();
        _disposed = true;
    }

    private Entries EntriesFromEntity(IEnumerable<EntryEntity> list)
    {
        var entries = list.Select(e => EntryFromEntity(e));
        return new Entries(entries);
    }

    private Entry EntryFromEntity(EntryEntity entry)
    {
        return new Entry(
            entry.Id,
            new BoundedPeriod(entry.Start, entry.End),
            entry.Project,
            entry.Task,
            entry.Description
        );
    }

}
