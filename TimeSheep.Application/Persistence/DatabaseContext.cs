using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace TimeSheep.Application.Persistence;

public partial class DatabaseContext(DbContextOptions options) : DbContext(options)
{
    public virtual DbSet<EntryEntity> Entries { get; set; }

    public virtual DbSet<ProjectEntity> Projects { get; set; }

    public static DatabaseContext UseSqlite(string databasePath)
    {
        var builder = new DbContextOptionsBuilder();
        builder.UseSqlite(databasePath);
        return new DatabaseContext(builder.Options);
    }

    public static DatabaseContext UsePostgres(string connectionString)
    {
        AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);

        var builder = new DbContextOptionsBuilder();
        builder.UseNpgsql(connectionString);
        return new DatabaseContext(builder.Options);
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfiguration(new EntryEntityTypeConfiguration());
    }

    private class EntryEntityTypeConfiguration : IEntityTypeConfiguration<EntryEntity>
    {
        public void Configure(EntityTypeBuilder<EntryEntity> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Start);
            builder.Property(x => x.End);
            builder.Property(x => x.Project);
            builder.Property(x => x.Task);
            builder.Property(x => x.Description);
        }
    }
}
