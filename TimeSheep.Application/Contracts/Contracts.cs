using System.Globalization;

namespace TimeSheep.Application;

public static class Contracts
{
    public static class V1
    {
        public class Create
        {
            public required string Start { get; set; }
            public required string End { get; set; }
            public required string Project { get; set; }
            public string? Task { get; set; }
            public string? Description { get; set; }
            public string Culture { get; set; } = CultureInfo.InvariantCulture.ToString();
        }

        public class List
        {
            public string? FromDate { get; set; }
            public string? ToDate { get; set; }
            public bool Day { get; set; }
            public bool Week { get; set; }
            public bool Month { get; set; }
            public bool Year { get; set; }
            public CultureInfo Culture { get; set; } = CultureInfo.InvariantCulture;

            public void Validate()
            {
                var wholeFlagCount =
                    (Day ? 1 : 0) +
                    (Week ? 1 : 0) +
                    (Month ? 1 : 0) +
                    (Year ? 1 : 0);

                if (wholeFlagCount > 1)
                {
                    throw new ArgumentException("Cannot use more than one flag --day, --week, --month, and --year");
                }

                if (ToDate != null)
                {
                    if (Day)
                    {
                        throw new ArgumentException("Cannot use --day with --to");
                    }

                    if (Week)
                    {
                        throw new ArgumentException("Cannot use --week with --to");
                    }

                    if (Month)
                    {
                        throw new ArgumentException("Cannot use --month with --to");
                    }

                    if (Year)
                    {
                        throw new ArgumentException("Cannot use --year with --to");
                    }
                }
            }
        }
    }
}
