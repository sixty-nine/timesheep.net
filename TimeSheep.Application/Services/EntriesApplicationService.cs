using System.Globalization;
using TimeSheep.Application.Persistence.Repositories;
using TimeSheep.Domain.Extensions;
using static TimeSheep.Application.Contracts;

namespace TimeSheep.Application;

public class EntriesApplicationService(IEntriesRepository repo)
{
    public void Handle(object command)
    {
        switch (command)
        {
            case V1.Create cmd:
                var start = DateTimeExtensions.DateTimeFromString(cmd.Start, new CultureInfo(cmd.Culture));
                var end = DateTimeExtensions.DateTimeFromString(cmd.End, new CultureInfo(cmd.Culture));

                if (!start.HasValue || !end.HasValue)
                {
                    throw new ArgumentException("The start and end dates are required");
                }

                repo.AddEntry(start.Value, end.Value, cmd.Project.ToUpper(), cmd.Task, cmd.Description);
                break;

            default:
                throw new InvalidOperationException($"Command type {command.GetType().FullName} is unknown");
        }
    }


}
