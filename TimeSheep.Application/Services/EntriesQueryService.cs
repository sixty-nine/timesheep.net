using TimeSheep.Application.Persistence.Repositories;
using TimeSheep.Domain;
using TimeSheep.Domain.Extensions;
using static TimeSheep.Application.Contracts;

namespace TimeSheep.Application;

public class EntriesQueryService(IEntriesRepository repo)
{
    public Entries GetEntries(V1.List cmd)
    {
        return GetEntriesAsync(cmd).GetAwaiter().GetResult();
    }

    public Entries GetEntries(Period period)
    {
        return GetEntriesAsync(period).GetAwaiter().GetResult();
    }

    public async Task<Entries> GetEntriesAsync(V1.List cmd)
    {
        cmd.Validate();
        var period = GetPeriod(cmd);
        return await repo.GetEntries(period.StartDate, period.EndDate);
    }

    public async Task<Entries> GetEntriesAsync(Period period)
    {
        return await repo.GetEntries(period.StartDate, period.EndDate);
    }

    public ProjectStats GetStats(V1.List cmd)
    {
        return GetStatsAsync(cmd).GetAwaiter().GetResult();
    }

    public async Task<ProjectStats> GetStatsAsync(V1.List cmd)
    {
        cmd.Validate();
        var entries = await GetEntriesAsync(cmd);
        return GetStats(entries);
    }

    public async Task<ProjectStats> GetStatsAsync(Period period)
    {
        var entries = await GetEntriesAsync(period);
        return GetStats(entries);
    }

    public ProjectStats GetStats(Entries entries)
    {
        return new ProjectStats(entries.List);
    }

    public PresenceTable GetPresenceTable(V1.List cmd)
    {
        return GetPresenceTableAsync(cmd).GetAwaiter().GetResult();
    }

    public async Task<PresenceTable> GetPresenceTableAsync(V1.List cmd)
    {
        cmd.Validate();
        var entries = await GetEntriesAsync(cmd);
        return GetPresenceTable(entries);
    }

    public async Task<PresenceTable> GetPresenceTableAsync(Period period)
    {
        var entries = await GetEntriesAsync(period);
        return GetPresenceTable(entries);
    }

    public PresenceTable GetPresenceTable(Entries entries)
    {
        return new PresenceTable(entries.List);
    }

    public Period GetPeriod(V1.List settings)
    {
        return Period.FromDate(
            DateTimeExtensions.DateTimeFromString(settings.FromDate, settings.Culture),
            DateTimeExtensions.DateTimeFromString(settings.ToDate, settings.Culture),
            settings.Day,
            settings.Week,
            settings.Month,
            settings.Year
        );
    }
}
