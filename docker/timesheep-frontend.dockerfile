FROM node:18

COPY ./frontend /app
WORKDIR /app
RUN yarn
RUN yarn build
EXPOSE 3000
ENV NEXT_TELEMETRY_DISABLED 1
CMD yarn start
