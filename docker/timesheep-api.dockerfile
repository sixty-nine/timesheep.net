FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build-env
COPY . /Timesheep
WORKDIR /Timesheep

RUN --mount=type=cache,id=nuget,target=/root/.nuget/packages dotnet publish -c Release -o out TimeSheep.API

EXPOSE 8080

FROM mcr.microsoft.com/dotnet/aspnet:8.0
WORKDIR /Timesheep
COPY --from=build-env /Timesheep/out .
ENTRYPOINT ["dotnet", "TimeSheep.API.dll"]
